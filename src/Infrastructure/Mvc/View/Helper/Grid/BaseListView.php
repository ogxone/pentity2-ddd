<?php
/**
 * Created by PhpStorm.
 * User: vov4ik
 * Date: 15.05.15
 * Time: 12:00
 */

namespace Pentity2\Infrastructure\Mvc\View\Helper\Grid;

use Pentity2\Utils\ArrayUtils\ArrayUtils;
use Pentity2\Utils\helpers\HtmlHelper;
use Pentity2\Utils\helpers\HtmlHelpers;
use Zend\View\Helper\AbstractHelper;

abstract class BaseListView extends AbstractHelper
{
    public $dataProvider;
    /**
     * @var string the HTML content to be displayed when [[dataProvider]] does not have any data.
     */
    public $emptyText = 'Data not found';

    /**
     * @var array the HTML attributes for the container tag of the list view.
     * The "tag" element specifies the tag name of the container element and defaults to "div".
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $options = [];
    public $translate;
//    public $pager = [];
    public $sorter = [];
    public $showOnEmpty = true;
    public $layout = "{items}\n{pager}";
    public $pagination = true;
    public $pager;

    abstract public function renderItems();


    /**
     * Runs the widget.
     */
    public function run()
    {
        if ($this->showOnEmpty || $this->dataProvider->getTotalCount() > 0) {
            $content = preg_replace_callback("/{\\w+}/", function ($matches) {
                $content = $this->renderSection($matches[0]);
                return $content === false ? $matches[0] : $content;
            }, $this->layout);
        } else {
            $content = $this->emptyText;
        }
        if (empty($content) || $content == "\n") {
            $content = $this->emptyText;
        }

        $tag = ArrayUtils::remove($this->options, 'tag', 'div');

        echo HtmlHelper::tag($tag, $content, $this->options);
    }

    /**
     * Renders a section of the specified name.
     * If the named section is not supported, false will be returned.
     * @param string $name the section name, e.g., `{summary}`, `{items}`.
     * @return string|boolean the rendering result of the section, or false if the named section is not supported.
     */
    public function renderSection($name)
    {
        switch ($name) {
            case '{items}':
                return $this->renderItems();
            case '{pager}':
                return $this->renderPager();
            default:
                return false;
        }
    }


    /**
     * Renders the pager.
     * @return string the rendering result
     */
    public function renderPager()
    {
        if ($this->pagination === false || $this->dataProvider->getCount() <= 0) {
            return '';
        }
        $view = $this->getView();
        $pager = isset($this->pager) ? $this->pager : $view->plugin('linkPager');
        $pager->setAttributes(['dataProvider' => $this->dataProvider]);
        return $pager->run();
    }


    public function setAttributes(array $attributes)
    {
        foreach ($attributes as $key => $val) {
            $this->$key = $val;
        }
    }

    public function setDataProvider($provider)
    {
        $this->dataProvider = $provider;
    }

}