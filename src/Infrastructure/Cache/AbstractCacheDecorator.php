<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 04.09.15
 * Time: 13:17
 */

namespace Pentity2\Infrastructure\Cache;


use Pentity2\Infrastructure\Decorator\DecoratorInterface;
use Pentityt2\Infrastructure\Cache\Exception\CacheException;
use Zend\Cache\Storage\FlushableInterface;
use Zend\Cache\Storage\StorageInterface as ZendStorageInterface;

abstract class AbstractCacheDecorator implements StorageInterface, DecoratorInterface
{
    const RESERVED_PREFIX = 'internal-';

    protected $_cache;
    /**
     * @var $_serializer \Serializable
     */
    private $_serializer;

    public function __construct(ZendStorageInterface $storage)
    {
        $this->_cache = $storage;
    }

    public function getOrigin()
    {
        return $this->_cache;
    }

    public function setSerializer(Array $serializer)
    {
        if (is_callable($serializer[0]) && is_callable($serializer[1])) {
            $this->_serializer = $serializer;
        } else {
            throw new CacheException('Serializer have to have following format: [0 => serializer_callable, 1 => unserializer_callable]');
        }
    }

    protected function _serialize($value)
    {
        if ($this->_serializer === null) {
            return serialize($value);
        } else {
            return $this->_serializer[0]($value);
        }
    }

    protected function _unserialize($value)
    {
        if ($this->_serializer === null) {
            return unserialize($value);
        } else {
            return $this->_serializer[1]($value);
        }
    }

    protected function _validateKey($key)
    {
        if (is_numeric($key)) {
            $key = (string)$key;
        }
        if (!is_string($key)) {
            throw new CacheException(sprintf('Key have to be a string. %s given', gettype($key)));
        }
        if (0 === strpos($key, self::RESERVED_PREFIX)) {
            throw new CacheException(sprintf('Key begining from %s are reserved', self::RESERVED_PREFIX));
        }
    }

    public function removeItem($key)
    {
        return $this->_cache->removeItem($key);
    }

    public function removeItems(Array $keys)
    {
        return $this->_cache->removeItems($keys);
    }

    public function flush()
    {
        if ($this->_cache instanceof FlushableInterface) {
           return $this->_cache->flush();
        }
        throw new CacheException(sprintf('%s doesn\'t implements Zend\Cache\Storage\FlushableInterface', get_class($this->_cache)));
    }
}