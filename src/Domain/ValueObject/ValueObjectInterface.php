<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 30.03.2015
 * Time: 0:25
 */

namespace Pentity2\Domain\ValueObject;


interface ValueObjectInterface
{
    public function equals(ValueObjectInterface $value);
} 