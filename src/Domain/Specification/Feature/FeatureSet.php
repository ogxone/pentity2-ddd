<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 31.03.2015
 * Time: 8:31
 */

namespace Pentity2\Specification\Feature;

use Pentity2\Utils\Ciollection\Set\HashSet;

class FeatureSet extends HashSet
{
    public function __construct()
    {
        parent::__construct([$this, '_hashFunction'], __CLASS__);
    }

    private function _hashFunction($item)
    {
        return get_class($item);
    }
} 