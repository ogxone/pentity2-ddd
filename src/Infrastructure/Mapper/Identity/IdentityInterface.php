<?php
/**
 * Created by PhpStorm.
 * User: Оля
 * Date: 05.04.2015
 * Time: 15:19
 */

namespace Pentity2\Infrastructure\Mapper\Identity;

use Pentity2\Domain\Entity\EntityInterface;

interface IdentityInterface {
    public function getIdentity($type, $id);
    public function getIdentityByEntity(EntityInterface $entity);
} 