<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 29.03.2015
 * Time: 23:19
 */

namespace Pentity2\Domain\Entity;

use Pentity2\Domain\Exception\DomainException;
use Pentity2\Utils\Identifier\IdentifierCreatorTrait;

abstract class AbstractEntity implements EntityInterface
{
    use IdentifierCreatorTrait;

    protected $_idFieldName = '';
    protected $_entityFields = [];
    protected $_fields = [];

    public final function __construct(Array $fields = [])
    {
        $idName = $this->getIdFieldName();

        if (!empty($fields)) {
            foreach ($fields as $name => $value) {
                $this->$name = $value;
            }
        }

        if (!$this->__isset($idName)) {
            $this->$idName = null;
        }
    }

    public function getIdFieldName()
    {
        return $this->_idFieldName;
    }

    public function setField($name, $value)
    {
        return $this->__set($name, $value);
    }

    public function hasField($name)
    {
        return false !== array_search($name, $this->_entityFields);
    }

    public function getField($name)
    {
        return $this->__get($name);
    }

    public function removeField($name)
    {
        return $this->__unset($name);
    }

    public function isNewRecord()
    {
        $idField = $this->getIdFieldName();
        return !($this->__isset($idField) && !is_null($this->$idField));
    }

    public function getArrayCopy()
    {
        $fields = $this->_fields;
        if ($this->isNewRecord()) {
            unset($fields[$this->getIdFieldName()]);
        }
        return array_intersect_key(
            $this->_fields,
            array_flip($this->_entityFields)
        );
    }

    public function exchangeArray(Array $data)
    {
        foreach ($data as $key => $entry) {
            $this->setField($key, $entry);
        }
    }

    public function __set($name, $value)
    {

        $mutator = "set" . ucfirst(strtolower($name));
        if (method_exists($this, $mutator) &&
            is_callable(array($this, $mutator))) {
            $this->$mutator($value);
        } elseif ($name == $this->getIdFieldName()) {
            $this->setId($value);
        } else {
            $this->_fields[$name] = $value;
        }
        return $this;
    }

    public function __get($name)
    {
        $accessor = preg_replace_callback('/_(.?)/', function(Array $m){return strtoupper($m[1]);}, $name);
        $accessor = "get" . ucfirst($accessor);
        if (method_exists($this, $accessor) &&
            is_callable(array($this, $accessor))) {
            return $this->$accessor();
        }
        if (!$this->__isset($name)) {
            throw new \InvalidArgumentException("The field '$name' has not been set for this entity yet.");
        }
        return $this->_fields[$name];
    }

    public function __isset($name)
    {
        return array_key_exists($name, $this->_fields);
    }

    public function __unset($name)
    {
        if (!$this->__isset($name)) {
            throw new \InvalidArgumentException(sprintf("The field \"%s\" has not been set for this entity yet.", $name));
        }
        unset($this->_fields[$name]);
        return $this;
    }

    public function setId($id)
    {
        $idName = $this->getIdFieldName();
        if (!empty($this->_fields[$idName])) {
            throw new \BadMethodCallException("The ID for this user has been set already.");
        }
        $this->_fields[$idName] = $id;
        return $this;
    }

    public function getIdField()
    {
        return $this->getField(
            $this->getIdFieldName()
        );
    }

    public function newInstance(Array $fields = [])
    {
        return new static($fields);
    }

    public function getEntityNames()
    {
        return $this->_entityFields;
    }

    public function toArray()
    {
        return $this->_fields;
    }

    public function merge(AbstractEntity $entity, $overwrite = true)
    {
        if (!$this instanceof $entity) {
            throw new DomainException(sprintf('Merged entities are not the same class (%s != %s)', get_class($this), get_class($entity)));
        }
        foreach ($entity->toArray() as $fieldName => $value) {
            if (!isset($this->_fields[$fieldName])) {
                $this->_fields[$fieldName] = $value;
            } elseif ($overwrite) {
                $this->_fields[$fieldName] = $value;
            }
        }
    }

    public function isValid()
    {
        return true;
    }
}