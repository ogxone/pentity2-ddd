<?php
/**
 * Created by PhpStorm.
 * User: Оля
 * Date: 25.04.2015
 * Time: 15:09
 */

namespace Pentity2\Infrastructure\Mvc\View\Helper\FormRow\Factory;

use Pentity2\Infrastructure\Mvc\View\Helper\FormRow\FormRowHelper;
use Zend\I18n\Translator\TranslatorAwareTrait;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class FormRowFactory implements FactoryInterface
{
    use TranslatorAwareTrait;

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return new FormRowHelper(
            $serviceLocator->getServiceLocator()
            ->get('App\Translator')
        );
    }
}