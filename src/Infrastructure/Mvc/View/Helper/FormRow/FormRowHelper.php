<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 14.05.2015
 * Time: 8:17
 */

namespace Pentity2\Infrastructure\Mvc\View\Helper\FormRow;

use Zend\Form\View\Helper\FormRow as ZendFormRow;
use Zend\I18n\Translator\Translator;

class FormRowHelper extends ZendFormRow
{
    private $_translator;

    public function __construct(Translator $translator)
    {
        $this->setRenderErrors(false);
        $this->_translator = $translator;
    }

    public function getTranslator()
    {
        return $this->_translator;
    }
}