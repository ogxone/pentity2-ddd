<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 04.08.15
 * Time: 17:34
 */

namespace Pentity2\Infrastructure\Mapper;


interface FetchableInterface
{
    public function fetchById($id);
    public function fetchOne();
    public function fetchAll();
}