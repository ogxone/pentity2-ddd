<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 18.08.15
 * Time: 20:35
 */

namespace Pentity2\Infrastructure\Service\TransactionManager\Factory;



use Pentity2\Infrastructure\Service\TransactionManager\TransactionManager;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class TransactionManagerFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return new TransactionManager($serviceLocator->get('Db'));
    }
}