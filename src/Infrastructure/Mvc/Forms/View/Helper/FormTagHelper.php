<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 13.05.2015
 * Time: 13:15
 */

namespace Pentity2\Infrastructure\Mvc\Forms\View\Helper;

use Zend\Form\View\Helper\AbstractHelper;
use Zend\Form\ElementInterface;

class FormTagHelper extends AbstractHelper
{
    public function __invoke(ElementInterface $element = null)
    {
        return $this->render($element);
    }

    public function render(ElementInterface $element)
    {
        $tag = $element->getOption('tag');
        $action = $element->getOption('action');
        $text = $element->getOption('text');

        if ($text) {
            $text = $this->getTranslator()->translate($text);
        }

        $attributes = $element->getAttributes();

        $html = '';
        if (!$action || $action == 'open') {
            $html = '<' . $tag . ' ';
            foreach ($attributes as $name => $value) {
                $html .=  $name . '= "' . $value . '" ';
            }
            $html .= '>' . $text;
        }
        if (!$action || $action == 'close') {
            $html .= '</' . $tag . '>';
        }

        return $html;
    }
}
