<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 21.08.15
 * Time: 10:40
 */

namespace Pentity2\Infrastructure\Mapper;


use Pentity2\Infrastructure\Mapper\Identity\IdentityInterface;

interface IdentityMapAwareInterface
{
    public function setIdentityMap(IdentityInterface $identity);
}