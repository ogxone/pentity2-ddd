<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 25.06.15
 * Time: 17:31
 */

namespace Pentity2\Application\Service;


use Pentity2\Utils\ErrorHandler\StaticErrorHandler;
use Pentity2\Utils\Identifier\IdentifierCreatorInterface;
use Pentity2\Utils\Identifier\IdentifierCreatorTrait;

abstract class AbstractService implements ExecutableApplicationServiceInterface, IdentifierCreatorInterface
{
    use IdentifierCreatorTrait;

    public function execute(Array $params = [])
    {
        try {
            $this->_executeLogic($params);
        } catch (\RuntimeException $e) {
            StaticErrorHandler::addError($this->createIdentifier(), $e->getMessage());
            $this->_errorLogic();
            return false;
        }
        return true;
    }

    abstract protected function _executeLogic(Array $params);
    protected function _errorLogic(){}
}