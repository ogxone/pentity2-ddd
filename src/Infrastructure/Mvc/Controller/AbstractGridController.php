<?php
/**
 * Created by PhpStorm.
 * User: vov4ik
 * Date: 29.05.15
 * Time: 11:52
 */

namespace Pentity2\Infrastructure\Mvc\Controller;


use Pentity2\Infrastructure\Mvc\Forms\Service\FormBuildService;
use Zend\Form\Form;
use Zend\View\Model\ViewModel;

class AbstractGridController extends AbstractActionController implements GridControllerInterface
{
    public $entityName = null;
    public $repoName = null;
    public $redirectRoutName = null;

    public function createAction()
    {
        if (!empty($this->entityName)) {
            $request = $this->getRequest();
            $entity = new $this->entityName;
            $form = new FormBuildService();
            $form = $form->buildFromEntity($entity);
            if ($request->isPost()) {
                $form->setData($request->getPost());
                if ($form->isValid()) {
                    $repo = $this->_getRepoFactory()
                        ->createRepo('Default', $this->repoName);
                    $fields = $request->getPost()->toArray();
                    $saveEntity = $entity->newInstance($fields);
                    $repo->save($saveEntity);
                    $this->redirect()->toRoute($this->redirectRoutName);
                }
            }
            $url = $this->url()->fromRoute('grid/create_db_record');
            $this->_render($form, $url);
        }
        return $this->getResponse();
    }

    function updateAction()
    {
        if (!empty($this->entityName)) {
            $request = $this->getRequest();
            $id = $this->params('id');
            if (empty($id)) {
                throw new \Exception('Page not found', 404);
            }
            $entity = new $this->entityName;
            $repo = $this->_getRepoFactory()
                ->createRepo('Default', $this->repoName);
            $form = new FormBuildService();
            $form = $form->buildFromEntity($entity);

            if ($request->isPost()) {
                $form->setData($request->getPost());
                if ($form->isValid()) {
                    $fields = $request->getPost()->toArray();
                    $fields[$entity->getIdFieldName()] = $id;
                    $saveEntity = $entity->newInstance($fields);
                    $repo->save($saveEntity);
                    $this->redirect()->toRoute($this->redirectRoutName);
                    return true;
                }
            }
            $data = $repo->fetchById($id);
            $form->setData($data->toArray());
            $url = $request->getUri()->getPath();
            $this->_render($form, $url);
        }
        return $this->getResponse();
    }

    public function deleteAction()
    {
        $repo = $this->_getRepoFactory()
            ->createRepo('Default', $this->repoName);
        $entity = new $this->entityName;
        $id = $this->params('id');
        if (empty($id)) {
            throw new \Exception('Page not found', 404);
        }
        $entity->setId($id);
        $repo->delete($entity);
        $this->redirect()->toRoute($this->redirectRoutName);
    }

    private function _render(Form $form, $url)
    {
        $form->setAttribute('action', $url);
        $form->setAttribute('class', 'form');
        $form->prepare();
        $viewRender = $this->getServiceLocator()->get('ViewRenderer');
        $layout = new ViewModel();
        $layout->setTemplate("layout/layout");
        $layout->setVariable("content", $viewRender->form()->render($form));
        echo $viewRender->partial($layout);
    }
}