<?php

/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 28.08.15
 * Time: 14:05
 */
namespace Pentity2Test\Acl;


use Pentity2\Infrastructure\Mvc\Controller\Plugin\Acl\Acl;
use Pentity2\Infrastructure\Test\AbstractTestCase;

class AclTest extends AbstractTestCase
{
    /**
     * @var $_acl Acl
    */
    private $_acl;

    public function setUp()
    {
         $this->_acl = new Acl($this->_getResources(), $this->_getRoles(), $this->_getRolesInheritance(), $this->_getRules());
    }

    /**
     * @test
    */
    public function testRolesInheriteanceRules()
    {
        $this->assertEquals(false, $this->_acl->isAllowed('guest', 'auth_index_logout'));
    }

    private function _getResources()
    {
        return [
            0 => 'auth',
            1 => 'auth_index',
            2 => 'auth_index_index',
            3 => 'auth_index_logout',
            4 => 'common',
            5 => 'index',
            6 => 'index_index',
            7 => 'index_index_index',
            8 => 'build',
            9 => 'build_build',
            10 => 'build_build_build',
            11 => 'build_build_clear',
            12 => 'build_cache',
            13 => 'build_cache_cc',
            14 => 'tester',
            15 => 'tester_index',
            16 => 'tester_index_index',
            17 => 'tester_index_custom',
        ];
    }

    private function _getRules()
    {
        return [
            'admin' => [
                'allow' => [
                    null
                ]
            ],
            'guest' => [
                'deny' => [
                    null
                ],
                'allow' => [
                    'tester',
                    'build',
                    'auth_index_index',
                    'index',
                ]
            ],
            'member' => [
                'deny' => [
                    'auth_index_index',
                ],
                'allow' => [
                    'auth_index_logout'
                ]
            ]
        ];
    }

    private function _getRoles()
    {
        return [
            'guest',
            'member',
            'editor',
            'admin',
        ];
    }

    private function _getRolesInheritance()
    {
        return [
            'member'         => ['guest'],
            'editor'         => ['member']
        ];
    }
}