<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 04.08.15
 * Time: 17:13
 */

namespace Pentity2\Infrastructure\Mapper;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Update;
use Zend\Db\Sql\Delete;

interface SqlMapperInterface extends FetchableInterface
{
    /**
     * @return Sql
     * */
    public function getDao();
    /**
     * @return Select
     * */
    public function getSelect();
    /**
     * @return Update
     * */
    public function getUpdate();
    /**
     * @return Delete
     * */
    public function getDelete();

    /**
     * @param Select $select
     */
    public function setSelect(Select $select);
}