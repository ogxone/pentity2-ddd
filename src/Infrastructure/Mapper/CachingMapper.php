<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 12.05.2015
 * Time: 14:13
 */

namespace Pentity2\Infrastructure\Mapper;


use Pentity2\Infrastructure\Cache\StorageInterface;
use Pentity2\Infrastructure\Mapper\Exception\MapperException;
use Pentity2\Utils\Param\Param;


class CachingMapper implements MapperInterface, FetchableInterface
{
    /**@var $_cache StorageInterface */
    private $_cache;
    /**@var $_mapper MapperInterface */
    private $_mapper;

    public function __construct(StorageInterface $storage, MapperInterface $mapper)
    {
        if (!$mapper instanceof \Serializable) {
            throw new MapperException(sprintf('Mapper %s have to implement \Serializable to be able to be cached', get_class($mapper)));
        }
        $this->_cache  = $storage;
        $this->_mapper = $mapper;
    }

    public function fetchById($id, Array $params = [])
    {
        $key = $this->_createKey($id, $params);
        if (null === ($res = $this->_cache->getItem($key))) {
            $tags = Param::getArr('tags', $params);
            $res = $this->_mapper->fetchById($id);
            $this->_cache->setItem($key, $res, $tags);
        }
        return $res;
    }

    public function fetchOne(Array $params = [])
    {
        $key = $this->_createKey(['limit' => 1], $params);
        if (null === ($res = $this->_cache->getItem($key))) {
            $tags = Param::getArr('tags', $params);
            $res = $this->_mapper->fetchOne();
            $this->_cache->setItem($key, $res, $tags);
        }
        return $res;
    }

    public function fetchAll($limit = null, $offset = null, Array $params = [])
    {
        $key = $this->_createKey($limit, $offset, $params);
        if (null === ($res = $this->_cache->getItem($key))) {
            $tags = Param::getArr('tags', $params);
            $res = $this->_mapper->fetchAll($limit, $offset);
            $this->_cache->setItem($key, $res, $tags);
        }
        return $res;
    }

    private function _createKey()
    {
        return $this->_mapper->serialize() . md5(serialize(func_get_args()));
    }

    public function getCacheStorage()
    {
        return $this->_cache;
    }

    /**
     * proxying all unknown methods to decorated
    */
    public function __call($name, Array $args)
    {
        if (!method_exists($this->_mapper, $name)) {
            throw new \BadMethodCallException(sprintf('Undefined method %s called on decorated %s', $name, get_class($this->_mapper)));
        }
        return call_user_func_array([$this->_mapper, $name], $args);
    }

    public function save($entity)
    {
        return $this->_mapper->save($entity);
    }

    public function delete($entity)
    {
        return $this->_mapper->delete($entity);
    }

    public function exists($entity)
    {
        return $this->_mapper->exists($entity);
    }
}