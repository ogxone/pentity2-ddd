<?php
/**
 * Created by PhpStorm.
 * User: Оля
 * Date: 04.05.2015
 * Time: 1:49
 */

namespace Pentity2\Infrastructure\Service\MCA;


use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class MCAService implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    private $_module;
    private $_controller;
    private $_action;

    private $_computed = false;
    private $_is404 = false;

    private function _computeMCA()
    {
        if ($this->_computed) {
            return $this->_is404;
        }
        $routeMatch = $this->getServiceLocator()
            ->get('Application')
            ->getMvcEvent()
            ->getRouteMatch();
        if (null === $routeMatch) {
            $this->_is404 = true;
            return false;
        }

        $this->_action = $routeMatch->getParam('action');
        $chunks = explode('\\', $routeMatch->getParam('controller'));

        if ($modulesName = array_shift($chunks)) {
            $this->_module = strtolower($modulesName);
        }
        if ($controllerName = array_pop($chunks)) {
            $controllerName = lcfirst($controllerName);
            $controllerName = preg_replace_callback('/([A-Z])/', function(Array $m){return '-' . strtolower($m[1]);}, $controllerName);
            $this->_controller = strtolower($controllerName);
        }
        return true;
    }

    public function getM()
    {
        if (!$this->_computeMCA()) {
            return '';
        }
        return $this->_module;
    }

    public function getMC()
    {
        if (!$this->_computeMCA()) {
            return '';
        }
        return $this->_module . '_' . $this->_controller;
    }

    public function getMCA()
    {
        if (!$this->_computeMCA()) {
            return '';
        }
        return $this->_module . '_' . $this->_controller . '_' . $this->_action;
    }
} 