<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 29.03.2015
 * Time: 21:21
 */

namespace Pentity2\Application\Service;


interface TransactionalSessionInterface
{
    public function executeTransaction(callable $action);
} 