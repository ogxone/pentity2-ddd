<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 28.05.2015
 * Time: 11:48
 */

namespace Pentity2\Infrastructure\Service\TransactionManager;

use Zend\Db\Adapter\Adapter;

/**
 * Makes sure transaction was started(stopped) only one time.
 * Use it instead of direct adapter calls
 *
 * @author ogxone
 * */
class TransactionManager
{
    private $_adapter;
    protected static $_transactionLevel = 0;

    public function __construct(Adapter $adapter)
    {
        $this->_adapter = $adapter;
    }

    public function beginTransaction()
    {
        if (self::$_transactionLevel === 0) {
            $this->_adapter->getDriver()->getConnection()->beginTransaction();
        }
        self::$_transactionLevel++;
    }

    public function commit()
    {
        if ( self::$_transactionLevel === 1 ) {
            $this->_adapter->getDriver()->getConnection()->commit();
        }
        self::$_transactionLevel--;
    }

    public function rollback()
    {

        if ( self::$_transactionLevel === 1 ) {
            $this->_adapter->getDriver()->getConnection()->rollback();
        }
        self::$_transactionLevel--;
    }

    public function getTransactionLevel()
    {
        return self::$_transactionLevel;
    }

}