<?php
namespace Pentity2\Infrastructure\Mvc\Forms;

use Pentity2\Utils\Param\Param;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerAwareTrait;
use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

abstract class AbstractForm extends Form implements EventManagerAwareInterface, ServiceLocatorAwareInterface
{
    use EventManagerAwareTrait, ServiceLocatorAwareTrait;

    protected $_useCsrf = true;
    protected $_config;
    protected $_inputFilter;

    const BEFORE_VALIDATE = 'beforeValidate';
    const AFTER_VALIDATE = 'afterValidate';

    public function __construct($name = null, $options = array())
    {
        $this->setServiceLocator(Param::strict('service_locator', $options));
        $this->setEventManager($this->getServiceLocator()->get('EventManager'));
        $this->_config = Param::strictArr('form_config', $options);
        parent::__construct($name, $options);
        $this->getEventManager()->attach(self::BEFORE_VALIDATE, [$this, self::BEFORE_VALIDATE]);
        $this->getEventManager()->attach(self::AFTER_VALIDATE, [$this, self::AFTER_VALIDATE]);
        $this->_init();
        $this->_applyCsrf();
    }

    abstract protected function _init();

    public function isValid()
    {
        $this->getEventManager()->trigger(self::BEFORE_VALIDATE);
        $valid = parent::isValid();
        $this->getEventManager()->trigger(self::AFTER_VALIDATE);
        return $valid;
    }

    public function beforeValidate()
    {
        return true;
    }

    public function afterValidate()
    {
        return true;
    }

    private function _applyCsrf()
    {
        if (!$this->_useCsrf) {
            return ;
        }
        $this->add([
            'type' => 'Csrf',
            'name' => 'security',
            'options' => [
                'csrf_options' => [
                    'timeout' => $this->_config['csrf_timeout']
                ]
            ]
        ]);
    }

    public function getConfig()
    {
        return $this->_config;
    }

    protected function _getTag($tag, $name, $label = '', $options = [], $attributes = [])
    {
        $attributes['id'] = empty($attributes['id']) ? $name : $attributes['id'];
        $options['tag'] = $tag;
        $options['text'] = $label;
        $this->add([
            'type' => 'Pentity2\Infrastructure\Mvc\Forms\Element\Tag',
            'options' => $options,
            'name' => $name,
            'attributes' => $attributes,
        ]);
        return $this;
    }

    public function getInputFilter()
    {
        if (!$this->_inputFilter) {
            $this->_inputFilter = new InputFilter;
            $this->_configureInputFilter($this->_inputFilter);
        }
        $parentInputFilter = parent::getInputFilter();
        foreach ($parentInputFilter->getInputs() as $name => $input) {
            if (!$this->_inputFilter->has($name)) {
                $this->_inputFilter->add($input, $name);
            }
        }
        return $this->_inputFilter;
    }

    protected function _configureInputFilter(InputFilterInterface $inputFilter)
    {

    }
}