<?php
/**
 * Created by PhpStorm.
 * User: Оля
 * Date: 25.04.2015
 * Time: 15:09
 */

namespace Pentity2\Infrastructure\Mvc\View\Helper\Lang\Factory;


use Pentity2\Infrastructure\Mvc\View\Helper\Lang\LangHelper;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class LangFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return new LangHelper(
            $serviceLocator->getServiceLocator()
                ->get('App\Translator')
        );
    }
}