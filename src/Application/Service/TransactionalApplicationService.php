<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 29.03.2015
 * Time: 21:20
 */

namespace Pentity2\Application\Service;


class TransactionalApplicationService implements ExecutableApplicationServiceInterface
{
    private $_session;
    private $_service;

    public function __construct(ExecutableApplicationServiceInterface $service, TransactionalSessionInterface $session)
    {
        $this->_service = $service;
        $this->_session = $session;
    }

    public function execute(Array $params = [])
    {
        $action = function () use ($params){
            return $this->_service->execute($params);
        };
        $this->_session->executeTransaction($action);
    }
}