<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 05.04.2015
 * Time: 19:46
 */

namespace Pentity2\Domain\Repo\Factory;


interface RepoFactoryInterface
{
    public function createRepo($name, Array $params = []);
} 