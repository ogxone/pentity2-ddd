<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 07.09.15
 * Time: 16:42
 */

namespace Pentity2\Infrastructure\Decorator;


interface DecoratorInterface 
{
    public function getOrigin();
}