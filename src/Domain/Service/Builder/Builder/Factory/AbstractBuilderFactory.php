<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 09.09.15
 * Time: 14:30
 */

namespace Pentity2\Domain\Service\Builder\Builder\Factory;


use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\MutableCreationOptionsInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

abstract class AbstractBuilderFactory implements FactoryInterface, MutableCreationOptionsInterface
{
    protected $_creationOptions = [];

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $builder = $this->_createBuilderLogic($serviceLocator);
        return $builder;
    }

    /**
     * Set creation options
     *
     * @param  array $options
     * @return void
     */
    public function setCreationOptions(Array $options)
    {
        $this->_creationOptions = $options;
    }

    protected abstract function _createBuilderLogic(ServiceLocatorInterface $sl);
}