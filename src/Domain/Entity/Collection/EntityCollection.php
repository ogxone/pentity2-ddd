<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 29.03.2015
 * Time: 22:23
 */

namespace Pentity2\Domain\Entity\Collection;

use Pentity2\Domain\Entity\EntityInterface;
use Pentity2\Utils\Collection\Exception\CollectionException;
use Pentity2\Domain\Entity\AbstractEntity;

class EntityCollection implements EntityCollectionInterface
{
    protected $_entities;
    protected $_count = 0;
    protected $_type;

    public function __construct(Array $entities = [], $type = null)
    {
        if (!empty($entities)) {
            foreach ($entities as $entity) {
                $this->add($entity);
            }
        }
    }

    private function _checkType(&$object)
    {
        if (null !== $this->_type && !$object instanceof $this->_type) {
            throw new CollectionException(
                sprintf('Object of type `%s` can\'t be added into objects collection of type `%s`', get_class($object), $this->_type));
        }
    }

    public function add(EntityInterface $entity)
    {
        $this->offsetSet($this->_count++, $entity);
    }

    public function remove(EntityInterface $entity)
    {
        $this->offsetUnset($entity);
    }

    public function get($key)
    {
        return $this->offsetGet($key);
    }

    public function exists($key)
    {
        return $this->offsetExists($key);
    }

    public function clear()
    {
        $this->_entities = array();
    }

    public function toArray()
    {
        if (empty($this->_entities)) {
            return [];
        }
        $ret = [];
        foreach ($this->_entities as $name => $entity) {
            $ret[$name] = $entity->toArray();
        }
        return $ret;
    }

    public function count()
    {
        return count($this->_entities);
    }

    public function offsetSet($key, $entity)
    {
        $this->_checkType($entity);
        if (!$entity instanceof EntityInterface) {
            throw new \InvalidArgumentException("Could not add the entity to the collection.");
        }
        if (!isset($key)) {
            $this->_entities[] = $entity;
        } else {
            $this->_entities[$key] = $entity;
        }
    }

    public function offsetUnset($key)
    {
        if ($key instanceof EntityInterface) {
            $this->_entities = array_filter($this->_entities,
                function ($v) use ($key) {
                    return $v !== $key;
                });
        } else if (isset($this->_entities[$key])) {
            unset($this->_entities[$key]);
        }
    }

    public function offsetGet($key)
    {
        if (isset($this->_entities[$key])) {
            return $this->_entities[$key];
        }
        return false;
    }

    public function offsetExists($key)
    {
        return $key instanceof EntityInterface
            ? array_search($key, $this->_entities)
            : isset($this->_entities[$key]);
    }

    public function getIterator()
    {
        return new \ArrayIterator($this->_entities);
    }

    public function getEntities()
    {
        return $this->_entities;
    }

    public function getEntitiesFields()
    {
        $attributes = [];
        $class = null;
        foreach ($this->_entities as $key => $val) {
            $curentClass = get_class($val);
            if ($class != $curentClass) {
                $attributes = $val->getEntityNames();
            }
        }
        return array_unique($attributes);
    }

    public function find($field, $value)
    {
        foreach ($this as $entity) {
            /**@var $entity AbstractEntity*/
            if ($entity->$field == $value) {
                return $entity;
            }
        }
        return null;
    }
} 