<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 09.09.15
 * Time: 20:57
 */

namespace Pentity2\Infrastructure\Mvc\View\Helper\Config\Factory;

use Pentity2\Infrastructure\Mvc\View\Helper\Config\ConfigHelper;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ConfigFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return new ConfigHelper(
            $serviceLocator->getServiceLocator()->get('Config')
        );
    }
}