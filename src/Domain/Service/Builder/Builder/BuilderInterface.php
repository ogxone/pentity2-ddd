<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 08.09.15
 * Time: 20:17
 */

namespace Pentity2\Domain\Service\Builder\Builder;


interface BuilderInterface
{
    public function getResult();
}