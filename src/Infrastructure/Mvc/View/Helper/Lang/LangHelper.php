<?php
/**
 * Created by PhpStorm.
 * User: Оля
 * Date: 04.05.2015
 * Time: 20:22
 */

namespace Pentity2\Infrastructure\Mvc\View\Helper\Lang;

use Zend\I18n\Translator\Translator;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\View\Helper\AbstractHelper;

class LangHelper extends AbstractHelper
{
    private $_translator;

    public function __construct(Translator $translator)
    {
        $this->_translator = $translator;
    }

    public function __invoke()
    {
        return $this->getCurrentLang();
    }

    /**
     * @returns string
     */
    public function getCurrentLang()
    {
        return $this->_translator->getLocale();
    }
} 