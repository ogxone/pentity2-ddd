<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 30.03.2015
 * Time: 9:58
 */

namespace Pentity2\Domain\Repo;


use Pentity2\Domain\Entity\Collection\EntityCollection;
use Pentity2\Domain\Entity\EntityInterface;

interface RepoInterface
{
    /**
     * @param $entity EntityInterface|EntityCollection
    */
    public function exists($entity);
    /**
     * @param $entity EntityInterface|EntityCollection
     */
    public function save($entity);
    /**
     * @param $entity EntityInterface|EntityCollection
     */
    public function delete($entity);

    public function fetchById($id, Array $params = []);
    public function fetchOne(Array $params = []);
    public function fetchAll(Array $params = []);
    public function fetchWithMax($attribute);
    public function fetchWithMin($attribute, Array $params = []);
    public function updateByAttribute(array $where, array $set);
} 