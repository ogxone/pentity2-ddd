<?php
namespace Pentity2\Infrastructure\Mvc\View\Helper\Grid;


use Pentity2\Utils\ArrayUtils\ArrayUtils;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class ListViewHelper extends BaseListView implements ServiceLocatorAwareInterface{

    use ServiceLocatorAwareTrait;

    public $itemOptions = [];
    public $itemView;
    public $viewParams = [];
    public $separator = "\n";
    public $options = ['class' => 'list-view'];

    public function renderItems()
    {
        $data = $this->dataProvider->getData();
        $rows = [];

        if(!empty($data)) {
            foreach (array_values($data->toArray()) as $index => $model) {
                $rows[] = $this->renderItem($model, null, $index);//$entities;
            }
        }
        return implode($this->separator, $rows);
    }

    public function renderItem($model, $key=null, $index)
    {
        if ($this->itemView === null) {
            $content = $key;
        } elseif (is_string($this->itemView)) {
            $content = $this->getView()->render($this->itemView, array_merge([
                'model' => $model,
            ], $this->viewParams));
        } else {
            $content = call_user_func($this->itemView, $model, $key, $index, $this);
        }
        $options = $this->itemOptions;
        $tag = ArrayUtils::remove($options, 'tag', 'div');
        if ($tag !== false) {
            $options['data-key'] = is_array($key) ? json_encode($key, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE) : (string) $key;

            return HtmlHelper::tag($tag, $content, $options);
        } else {
            return $content;
        }
    }


}