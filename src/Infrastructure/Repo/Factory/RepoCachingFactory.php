<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 12.05.2015
 * Time: 10:16
 */

namespace Pentity2\Infrastructure\Repo\Factory;


use Pentity2\Domain\Repo\Factory\RepoFactoryInterface;
use Zend\Cache\Pattern\PatternInterface;
use Pentity2\Infrastructure\Repo\AbstractRepo;

class RepoCachingFactory implements RepoFactoryInterface
{
    private $_cache;

    public function __construct(PatternInterface $cache)
    {
        $this->_cache = $cache;
    }

    public function createRepo($name, Array $params = [])
    {
        /**@var $repo AbstractRepo*/
        $repo = $this->_cache->createRepo($name, $params);
        return $repo;
    }
}