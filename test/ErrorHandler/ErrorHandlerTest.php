<?php

/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 31.08.15
 * Time: 13:35
 */
namespace Pentity2Test\ErrorHandler;


use Pentity2\Infrastructure\Test\AbstractTestCase;
use Pentity2\Utils\ErrorHandler\ErrorHandler;

class ErrorHandlerTest extends AbstractTestCase
{
    use ErrorHandler;
    /**
     * @test
    */
    public function checkSimpleAddAndGetToStorage()
    {
        $this->addError('testError', 'testErrorMessage1');
        $this->assertEquals($this->getError('testError'), ['testErrorMessage1']);
        $this->addError('testError', 'testErrorMessage2');
        $this->assertEquals($this->getError('testError'), ['testErrorMessage1', 'testErrorMessage2']);
        $this->assertEquals($this->getAllErrors(), new \ArrayObject(['testError' => ['testErrorMessage1', 'testErrorMessage2']]));
        $this->addError('testError2', 'testError2Message1');
        $this->assertEquals($this->getError('testError'), ['testErrorMessage1', 'testErrorMessage2']);
        $this->assertEquals($this->getError('testError2'), ['testError2Message1']);
        $this->assertEquals($this->getAllErrors(), new \ArrayObject([
            'testError' => ['testErrorMessage1', 'testErrorMessage2'],
            'testError2' => ['testError2Message1']
        ]));
    }

    /**
     * @test
    */
    public function checkClearStorage()
    {
        $this->addError('testError', 'testErrorMessage1');
        $this->addError('testError', 'testErrorMessage2');
        $this->addError('testError2', 'testErrorMessage3');
        $this->addError('testError2', 'testErrorMessage4');
        $this->addError('testError3', 'testErrorMessage5');

        $this->clearError('testError');

        $this->assertEquals($this->getAllErrors(), new \ArrayObject([
            'testError'  => [],
            'testError2' => ['testErrorMessage3', 'testErrorMessage4'],
            'testError3' => ['testErrorMessage5']
        ]));
    }

    /**
     * @test
    */
    public function checkHasStorage()
    {
        $this->addError('testError4', 'testErrorMessage1');
        $this->assertEquals($this->hasError('testError'), true);
        $this->assertEquals($this->hasError('testError5'), false);
    }
}