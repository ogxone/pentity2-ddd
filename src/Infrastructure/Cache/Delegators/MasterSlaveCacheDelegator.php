<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 04.09.15
 * Time: 16:04
 */

namespace Pentity2\Infrastructure\Cache\Delegators;


use Pentity2\Infrastructure\Cache\MasterSlaveCache;
use Zend\ServiceManager\DelegatorFactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class MasterSlaveCacheDelegator implements DelegatorFactoryInterface
{

    /**
     * A factory that creates delegates of a given service
     *
     * @param ServiceLocatorInterface $serviceLocator the service locator which requested the service
     * @param string $name the normalized service name
     * @param string $requestedName the requested service name
     * @param callable $callback the callback that is responsible for creating the service
     *
     * @return mixed
     */
    public function createDelegatorWithName(ServiceLocatorInterface $serviceLocator, $name, $requestedName, $callback)
    {
        $decorated = call_user_func($callback);
        return new MasterSlaveCache($decorated);
    }
}