<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 30.03.2015
 * Time: 1:26
 */

namespace Pentity2\Domain\Event;


class DomainEventPublisher 
{
    protected static  $_instance;
    private $_subscribers;
    private $_id = 0;

    public function __construct()
    {
        $this->_subscribers = [];
    }

    public static function getInstance()
    {
        if (null === static::$_instance) {
            static::$_instance = new self();
        }

        return static::$_instance;
    }

    public function __clone()
    {
        throw new \BadMethodCallException('Clone is not supported');
    }

    public function subscribe(DomainEventSubscriberInterface $subscriber)
    {
        $id = $this->_id;
        $this->_subscribers[$id] = $subscriber;
        $this->_id++;
        return $id;
    }

    public function unsubscribe($id)
    {
        if ($id instanceof DomainEventSubscriberInterface) {
            $this->_subscribers = array_filter($this->_subscribers,
                function ($v) use ($id) {
                    return $v !== $id;
                });
        }
        else if (isset($this->_subscribers[$id])) {
            unset($this->_subscribers[$id]);
        }
    }

    public function publish(DomainEventInterface $event)
    {
        foreach ($this->_subscribers as $subscriber) {
            /**@var $subscriber DomainEventSubscriberInterface*/
            if ($subscriber->isSubscribedTo($event)) {
                $subscriber->handle($event);
            }
        }
    }
} 