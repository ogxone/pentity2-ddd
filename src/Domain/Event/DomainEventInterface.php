<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 30.03.2015
 * Time: 1:22
 */

namespace Pentity2\Domain\Event;

interface DomainEventInterface
{
    public function occurredOn();
} 