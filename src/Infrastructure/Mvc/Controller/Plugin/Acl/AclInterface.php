<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 22.05.2015
 * Time: 8:24
 */

namespace Pentity2\Infrastructure\Mvc\Controller\Plugin\Acl;


interface AclInterface
{
    public function isAllowed($role, $resource);
    public function getAcl();
}