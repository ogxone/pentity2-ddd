<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 12.05.2015
 * Time: 12:53
 */

namespace Pentity2\Infrastructure\Mvc\Forms\Factory;


use Zend\Cache\Pattern\PatternInterface;

class FormCachingFactory implements FactoryInterface
{
    private $_cache;

    public function __construct(PatternInterface $cache)
    {
        $this->_cache = $cache;
    }

    public function createForm($name, Array $params = [])
    {
        return $this->_cache->createForm($name, $params);
    }

    public function createInputFilter($name, Array $params = [])
    {
        return $this->_cache->createInputFilter($name, $params);
    }
}