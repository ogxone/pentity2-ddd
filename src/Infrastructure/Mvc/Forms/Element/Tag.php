<?php

namespace Pentity2\Infrastructure\Mvc\Forms\Element;

use Zend\Form\Element;

class Tag extends Element
{
    public function setOptions($options) {
        $options['required'] = false;
        parent::setOptions($options);
    }
}