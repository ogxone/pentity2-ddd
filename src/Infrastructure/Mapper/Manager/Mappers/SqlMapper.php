<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 30.03.2015
 * Time: 9:55
 */

namespace Pentity2\Infrastructure\Mapper\Manager\Mappers;

use Pentity2\Domain\Entity\Collection\EntityCollection;
use Pentity2\Domain\Entity\EntityInterface;
use Pentity2\Infrastructure\Mapper\AbstractMapper;
use Pentity2\Infrastructure\Mapper\CallProcedureInterface;
use Pentity2\Infrastructure\Mapper\Exception\MapperException;
use Pentity2\Infrastructure\Mapper\SqlMapperInterface;
use Pentity2\Infrastructure\Service\TransactionManager\TransactionManager;
use Pentity2\Infrastructure\Zf2\Db\Sql\Insert;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Adapter\Driver\Pdo\Result;
use Zend\Db\Adapter\Driver\StatementInterface;
use Zend\Db\Adapter\Exception\InvalidQueryException;
use Zend\Db\Sql\Delete;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\SqlInterface;
use Zend\Db\Sql\Update;

class SqlMapper extends AbstractMapper implements SqlMapperInterface, CallProcedureInterface
{
    protected $_transactionManager;
    protected $_dao;
    protected $_select;
    protected $_insert;
    protected $_update;
    protected $_delete;

    public function __construct(EntityInterface $entity, $storage,  Adapter $adapter, TransactionManager $transactionManager)
    {
        $this->_transactionManager = $transactionManager;
        $this->_adapter = $adapter;
        parent::__construct($entity, $storage);
    }

    /**
     * @param $id int entity id
     * @param array $params
     * @return EntityInterface|null
     */
    public function fetchById($id, Array $params = [])
    {
        $entity = $this->_entity->newInstance();
        if (false !== ($returnEntity = $this->_getIdentity(get_class($entity), $id))) {
            return $returnEntity;
        }

        $this->getSelect()
            ->where([$entity->getIdFieldName() => $id]);
        if (
            null !== ($rows = $this->_executeSelect()) &&
            $rows['count'] > 0
        ) {
            $returnEntity = $this->_loadEntity(current($rows['rowset']), $entity);
            $this->_addIdentity($returnEntity);
            return $returnEntity;
        }
        //nothing found
        return null;
    }

    /**
     * @param array $params
     * @return EntityInterface|null
     */
    public function fetchOne(Array $params = [])
    {
        $entity = $this->_entity->newInstance();
        $this->getSelect()
            ->limit(1);
        if (
            null !== ($rows = $this->_executeSelect()) &&
            $rows['count'] > 0
        ) {
            return $this->_loadEntity(current($rows['rowset']), $entity);
        }
        //nothing found
        return null;
    }

    /**
     * @param $limit int
     * @param $offset int
     * @param array $params
     * @return EntityCollection|null
     */
    public function fetchAll($limit = null, $offset = null, Array $params = [])
    {
        if (null !== $limit) {
            $this->getSelect()
                ->limit((int)$limit);
            if (null !== $offset) {
                $this->getSelect()
                    ->offset((int)$offset);
            }
        }
        if (
            null !== ($rows = $this->_executeSelect()) &&
            $rows['count'] > 0
        ) {
            return $this->_loadEntityCollection($rows['rowset']);
        }
        //nothing found
        return null;
    }

    /**
     * @param $procedureName
     * @param array $callParams
     * @param array $params
     * @return EntityCollection|null
     */
    public function callProcedure($procedureName, Array $callParams, Array $params = [])
    {
        $callParams = implode(',', array_map(function ($v) {
            if (is_string($v)) {
                return $this->_adapter->getPlatform()->quoteValue($v);
            } else {
                return $v;
            }
        }, $callParams));

        $stmt = $this->_adapter->createStatement("CALL $procedureName($callParams);");
        $stmt->prepare();
        if (
            null !== ($rows = $this->_executeProcedure($stmt)) &&
            $rows['count'] > 0
        ) {
            return $this->_loadEntityCollection($rows['rowset']);
        }
        //nothing found
        return null;
    }

    /**
     * @param EntityCollection|EntityInterface $entity
     * @return array|string depending on input
     * @throws MapperException|InvalidQueryException
     */
    protected function _insert(EntityInterface $entity)
    {
        $idName = $entity->getIdFieldName();
        if (!$entity->isNewRecord()) {
            throw new InvalidQueryException(
                sprintf('Can\'t insert given entity(class:%s, id: %s) because it is new', get_class($entity), $entity->$idName));
        }
        $this->getInsert()
            ->values($entity->getArrayCopy());
        $id = $this->_executeInsert();
        if ($id > 0) {
            $entity->setField($idName, $id);
        }
        return $id;
    }

    protected function _update(EntityInterface $entity)
    {
        $idName = $entity->getIdFieldName();
        if ($entity->isNewRecord()) {
            throw new InvalidQueryException(
                sprintf('Can\'t update given entity(class:%s) because it is new', get_class($entity)));
        }
        $this->getUpdate()
            ->set($entity->getArrayCopy())
            ->where([$idName => $entity->$idName]);
        return $this->_executeUpdate();
    }

    public function updateByAttribute(array $where, array $set)
    {
        $update = $this->getUpdate();
        $update->where($where)
            ->set($set);
        return $this->_executeUpdate();

    }

    protected function _existsLogic(EntityInterface $entity)
    {
        $select = $this->getSelect();
        $select->columns([$entity->getIdFieldName()]);
        return null !== $this->fetchOne();
    }

    protected function _existsCollection(EntityCollection $collection)
    {
        $this->_transactionManager->beginTransaction();
        parent::_existsCollection($collection);
        $this->_transactionManager->commit();
    }

    protected function _saveLogic(EntityInterface $entity)
    {
        return $entity->isNewRecord()
            ? $this->_insert($entity)
            : $this->_update($entity);
    }

    protected function _saveCollection(EntityCollection $collection)
    {
        $this->_transactionManager->beginTransaction();
        parent::_saveCollection($collection);
        $this->_transactionManager->commit();
    }

    protected function _deleteLogic(EntityInterface $entity)
    {
        $idName = $entity->getIdFieldName();
        if ($entity->isNewRecord()) {
            throw new InvalidQueryException(
                sprintf('Can\'t delete given entity(class:%s) because it is new', get_class($entity)));
        }
        $this->getDelete()
            ->where([$idName => $entity->$idName]);
        return $this->_executeDelete();
    }

    protected function _deleteCollection(EntityCollection $collection)
    {
        $this->_transactionManager->beginTransaction();
        parent::_deleteCollection($collection);
        $this->_transactionManager->commit();
    }

    private function _executeSelect(Sql $sql = null)
    {
        return $this->_execute(function () use ($sql) {
            return null === $sql ? $this->getSelect() : $sql;
        }, function (Result $result) {
            return [
                'rowset' => $result->getResource()->fetchAll(\PDO::FETCH_ASSOC),
                'count' => $result->count(),
            ];
        });
    }

    private function _executeInsert(Insert $sql = null)
    {
        return $this->_execute(function () use ($sql) {
            return null === $sql ? $this->getInsert() : $sql;
        }, function (Result $result) {
            return $this->_adapter->getDriver()->getConnection()->getResource()->lastInsertId();;
        });
    }

    private function _executeUpdate(Update $sql = null)
    {
        return $this->_execute(function () use ($sql) {
            return null === $sql ? $this->getUpdate() : $sql;
        }, function (Result $result) {
            return $result->getAffectedRows();
        });
    }

    private function _executeDelete(Delete $sql = null)
    {
        return $this->_execute(function () use ($sql) {
            return null === $sql ? $this->getDelete() : $sql;
        }, function (Result $result) {
            return $result->getResource()->queryString;
        });
    }

    private function _executeProcedure(StatementInterface $stmt)
    {
        return $this->_execute(function () use ($stmt) {
            return $stmt;
        }, function (Result $result) {
            return [
                'rowset' => $result->getResource()->fetchAll(\PDO::FETCH_ASSOC),
                'count' => $result->count(),
            ];
        });
    }

    private function _execute(callable $getSqlFunc, callable $fetchResultsFunc)
    {
        $sql = $getSqlFunc();
        if ($sql instanceof StatementInterface) {
            $statement = $sql;
            $sql = $sql->getSql();
        } else {
            $statement = $this->_adapter->createStatement(
                $this->getDao()->buildSqlString($sql)
            );
        }
        try {
            $this->_onQueryBeforeExecute($sql);
            $result = $statement->execute();
            $return = $fetchResultsFunc($result);
            $this->_clear(); //@todo fix this duplication when migrate to >=5.5
        } catch (InvalidQueryException $e) {
            $this->_onBuildResultFailure($sql, $e);
            $this->_clear();
            throw $e;
        }
        return $return;
    }

    protected function _dumpQuery($query)
    {
        return $query instanceof SqlInterface ? $this->getDao()->buildSqlString($query) : $query;
    }

    private function _clear()
    {
        $this->_delete =
        $this->_update =
        $this->_insert =
        $this->_select = null;
    }

    /**
     * @return Sql
     */
    public function getDao()
    {
        if (null === $this->_dao) {
            $this->_dao = new Sql($this->_adapter);
        }
        return $this->_dao;
    }

    /**
     * @return Select
     */
    public function getSelect()
    {
        if (null === $this->_select) {
            $this->_select = (new Select())->from([$this->_entityTable[0] => $this->_entityTable]);
        }
        return $this->_select;
    }

    /**
     * @return Insert
     */
    public function getInsert()
    {
        if (null === $this->_insert) {
            $this->_insert = new Insert($this->_entityTable);
        }
        return $this->_insert;
    }

    /**
     * @return Update
     */
    public function getUpdate()
    {
        if (null === $this->_update) {
            $this->_update = new Update($this->_entityTable);
        }
        return $this->_update;
    }

    /**
     * @return Delete
     */
    public function getDelete()
    {
        if (null === $this->_delete) {
            $this->_delete = new Delete($this->_entityTable);
        }
        return $this->_delete;
    }

    public function setSelect(Select $select)
    {
        $this->_select = $select;
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     */
    public function serialize()
    {
        return md5(
            $this->getDao()->getSqlStringForSqlObject(
                $this->getSelect()
            )
        );
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @throws MapperException
     */
    public function unserialize($serialized)
    {
        throw new MapperException(sprintf('%s not implemented yet', __METHOD__));
    }
}