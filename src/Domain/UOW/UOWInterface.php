<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 30.03.2015
 * Time: 9:09
 */

namespace Pentity2\Domain\UOW;

use Pentity2\Domain\Entity\EntityInterface;

interface UnitOfWorkInterface
{
    public function registerNew(EntityInterface $entity);
    public function registerClean(EntityInterface $entity);
    public function registerDirty(EntityInterface $entity);
    public function registerDeleted(EntityInterface $entity);
    public function commit();
    public function rollback();
    public function clear();
} 