<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 29.03.2015
 * Time: 21:03
 */

namespace Pentity2\Infrastructure\Mapper;

use Pentity2\Domain\Entity\EntityInterface;
use Pentity2\Domain\Entity\AbstractEntity;
use Pentity2\Domain\Entity\Collection\EntityCollection;
use Pentity2\Infrastructure\Mapper\Exception\MapperException;
use Pentity2\Infrastructure\Mapper\Identity\IdentityInterface;
use Pentity2\Infrastructure\Mapper\Identity\IdentityMap;
use Pentity2\Utils\ErrorHandler\StaticErrorHandler;
use Zend\EventManager\EventManagerAwareTrait;
use Zend\Stdlib\Hydrator\ArraySerializable;
use Zend\Stdlib\Hydrator\HydratorAwareInterface;
use Zend\Stdlib\Hydrator\HydratorInterface;

abstract class AbstractMapper implements MapperInterface, HydratorAwareInterface, IdentityMapAwareInterface,\Serializable
{
    use EventManagerAwareTrait;

    const EVENT_BEFORE_EXECUTE = 'mapper.db.beforeexecute';
    const EVENT_DB_FAILURE = 'mapper.db.failure';
    const IDENTIFIER = 'Mapper\Identifier';

    protected $_adapter;
    /** @var $_entity AbstractEntity*/
    protected $_entity;
    protected $_entityTable;
    /** @var $_hydrator HydratorInterface*/
    protected $_hydrator;

    /** @var $_identityMap IdentityMap*/
    protected $_identityMap;
    protected $eventIdentifier = self::IDENTIFIER;

    public function __construct(EntityInterface $entity, $storage)
    {
        $this->setEntity($entity);
        $this->setStorage($storage);
    }

    public function getEventManager()
    {
        return $this->events;
    }

    public function getAdapter()
    {
        return $this->_adapter;
    }

    public function setIdentityMap(IdentityInterface $identity)
    {
        $this->_identityMap = $identity;
    }

    public function setStorage($entityTable) {
        if (!is_string($entityTable) || empty($entityTable)) {
            throw new \InvalidArgumentException("The entity table is invalid.");
        }
        $this->_entityTable = $entityTable;
        return $this;
    }

    public function setEntity(EntityInterface $entity)
    {
        $this->_entity = $entity;
    }

    protected function _loadEntityCollection(array $rows) {
        $collection = new EntityCollection;
        foreach ($rows as $row) {
            $collection->add($this->_loadEntity($row));
        }
        return $collection;
    }

    public function getHydrator()
    {
        if (null === $this->_hydrator) {
            $this->_hydrator = new ArraySerializable;
        }
        return $this->_hydrator;
    }

    public function setHydrator(HydratorInterface $hydrator)
    {
        $this->_hydrator = $hydrator;
    }

    /**
     * @param EntityCollection|EntityInterface $entity
     * @return bool|null|int false - error, null - nothing found, int - last insert(update) id if insert
     * @throws MapperException if invalid input provided
     */
    public function exists($entity)
    {
        if ($entity instanceof EntityCollection) {
            return $this->_existsCollection($entity);
        } elseif (!$entity instanceof EntityInterface) {
            throw new MapperException('Entity have to be either EntityInterface or EntityCollection instance');
        }
        try {
            return $this->_existsLogic($entity);
        } catch (\RuntimeException $e) { //all kind of db exception including PDOException
            //@todo false on non exists and on error. Work around !
            StaticErrorHandler::addError($entity->createIdentifier(), $e->getMessage());
            return false;
        }
    }

    /**
     * @param EntityCollection|EntityInterface $entity
     * @return bool|null|int false - error, null - nothing found, int - last insert(update) id if insert
     * @throws MapperException if invalid input provided
     */
    public function save($entity)
    {
        if ($entity instanceof EntityCollection) {
            return $this->_saveCollection($entity);
        } elseif (!$entity instanceof EntityInterface) {
            throw new MapperException('Entity have to be either EntityInterface or EntityCollection instance');
        }
        try {
            return $this->_saveLogic($entity);
        } catch (\RuntimeException $e) { //all kind of db exception including PDOException
            StaticErrorHandler::addError($entity->createIdentifier(), $e->getMessage());
            return false;
        }
    }

    /**
     * @param EntityCollection|EntityInterface $entity
     * @return bool|string
     * @throws MapperException
     */
    public function delete($entity)
    {
        if ($entity instanceof EntityCollection) {
            return $this->_deleteCollection($entity);
        } elseif (!$entity instanceof EntityInterface) {
            throw new MapperException('Entity have to be either EntityInterface or EntityCollection instance');
        }
        try {
            return $this->_deleteLogic($entity);
        } catch (\RuntimeException $e) { //all kind of db exception including PDOException
            StaticErrorHandler::addError($entity->createIdentifier(), $e->getMessage());
            return false;
        }
    }

    protected function _saveCollection(EntityCollection $collection)
    {
        $opRes = new EntityCollection;
        foreach ($collection as $entity) {
            /** @var $entity EntityInterface*/
            if (false === $this->save($entity)) {
                $opRes->add($entity);
            }
        }
        return $opRes->count() !== 0 ? $opRes : true;
    }

    protected function _deleteCollection(EntityCollection $collection)
    {
        $opRes = new EntityCollection;
        foreach ($collection as $entity) {
            /** @var $entity EntityInterface*/
            if (false === $this->delete($entity)) {
                $opRes->add($entity);
            }
        }
        return $opRes->count() !== 0 ? $opRes : true;
    }

    protected function _existsCollection(EntityCollection $collection)
    {
        $opRes = new EntityCollection;
        foreach ($collection as $entity) {
            /** @var $entity EntityInterface*/
            if (false === $this->exists($entity)) {
                $opRes->add($entity);
            }
        }
        return $opRes->count() !== 0 ? $opRes : true;
    }

    /**
     * @param array $row
     * @param EntityInterface $entity
     * @return EntityInterface
     */
    protected function _loadEntity(Array $row, EntityInterface $entity = null)
    {
        return $this->getHydrator()->hydrate(
            $row,
            $entity ? : $this->_entity->newInstance());
    }

    protected function _getIdentity($class, $id)
    {
        if (null !== $this->_identityMap) {
            return $this->_identityMap->getIdentity($class, $id);
        }
        return false;
    }

    protected function _addIdentity(EntityInterface $entity)
    {
        if (null !== $this->_identityMap) {
            $this->_identityMap->addEntityToMap($entity);
        }
    }

    protected function _onBuildResultFailure($query, \Exception $exception)
    {
        if (null !== ($eventManager = $this->getEventManager())) {
            $eventManager->trigger(self::EVENT_DB_FAILURE, $this, [
                'exception' => $exception,
                'query' => $this->_dumpQuery($query)
            ]);
        }
    }

    protected function _onQueryBeforeExecute($query)
    {
        if (null !== ($eventManager = $this->getEventManager())) {
            $eventManager->trigger(self::EVENT_BEFORE_EXECUTE, $this, [
                'query' => $this->_dumpQuery($query)
            ]);
        }
    }

    abstract protected function _existsLogic(EntityInterface $entity);
    abstract protected function _saveLogic(EntityInterface $entity);
    abstract protected function _deleteLogic(EntityInterface $entity);
    abstract protected function _dumpQuery($query);
}