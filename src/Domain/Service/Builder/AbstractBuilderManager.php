<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 09.09.15
 * Time: 14:02
 */

namespace Pentity2\Domain\Service\Builder;


use Common\Domain\Service\Builder\Builder\BuilderInterface;
use Pentity2\Domain\Service\Exception\ServiceException;
use Zend\ServiceManager\AbstractPluginManager;
use Zend\ServiceManager\Exception;

class AbstractBuilderManager extends AbstractPluginManager
{

    /**
     * Validate the plugin
     *
     * Checks that the filter loaded is either a valid callback or an instance
     * of FilterInterface.
     *
     * @param  mixed $plugin
     * @return void
     * @throws Exception\RuntimeException if invalid
     */
    public function validatePlugin($plugin)
    {
        if ($plugin instanceof BuilderInterface) {
            //we are ok
            return;
        }
        throw new ServiceException(sprintf(
           'Service %s expected to be of type Common\Domain\Service\Builder\Builder\BuilderInterface',
            get_class($plugin)
        ));
    }
}