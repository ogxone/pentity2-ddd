<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 04.09.15
 * Time: 14:28
 */

namespace Pentity2\Infrastructure\Cache;


interface StorageInterface
{
    public function setItem($key, $value, Array $tags = []);
    public function setItems(Array $keyValuePairs, Array $tags = []);
    public function getItem($key, & $success = null, & $casToken = null);
    public function getItems(Array $keys);
    public function clearByTags(Array $tags);
    public function hasItem($key);
    public function hasItems(Array $keys);
    public function removeItem($key);
    public function removeItems(array $keys);
    public function setSerializer(Array $serializer);
    public function getTags($key);
    public function flush();
}