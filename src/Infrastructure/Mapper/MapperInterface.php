<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 29.03.2015
 * Time: 20:57
 */

namespace Pentity2\Infrastructure\Mapper;

use Pentity2\Domain\Entity\Collection\EntityCollection;
use Pentity2\Domain\Entity\EntityInterface;

interface MapperInterface
{
    /**
     * @param $entity EntityInterface|EntityCollection
     */
    public function exists($entity);
    /**
     * @param $entity EntityInterface|EntityCollection
     */
    public function save($entity);
    /**
     * @param $entity EntityInterface|EntityCollection
     */
    public function delete($entity);
}