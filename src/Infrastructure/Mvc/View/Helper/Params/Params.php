<?php

namespace Pentity2\Infrastructure\Mvc\View\Helper\Params;

use Zend\Mvc\MvcEvent;
use Zend\Stdlib\RequestInterface;
use Zend\View\Helper\AbstractHelper;

class Params extends AbstractHelper
{
    protected $request;

    protected $event;

    public function __construct(RequestInterface $request, MvcEvent $event)
    {
        $this->request = $request;
        $this->event = $event;
    }

    public function fromPost($param = null, $default = null)
    {
        if ($param === null)
        {
            return $this->request->getPost($param, $default)->toArray();
        }

        return $this->request->getPost($param, $default);
    }

    public function fromRoute($param = null, $default = null)
    {
        $error = $this->event->getError();
        if (!empty($error)) {
            return $default;
        }

        if ($param === null)
        {
            return $this->event->getRouteMatch()->getParams();
        }
        return $this->event->getRouteMatch()->getParam($param, $default);
    }

    public function fromQuery($param = null, $default = null)
    {
        if ($param === null)
        {
            return $this->request->getQuery($param, $default)->toArray();
        }

        return $this->request->getQuery($param, $default);
    }

    /**
     * Return current module name
     *
     * @return return
     */
    public function moduleName()
    {
        $controller = $this->fromRoute('controller');
        $module = strtolower(substr($controller, 0, strpos($controller, '\\')));
        return $module;
    }

    /**
     * Return current controller name in lowercase
     *
     * @return string
     */
    public function controllerName()
    {
        $controller = $this->fromRoute('controller');
        $controller = explode('\\', $controller);
        $controller = lcfirst(end($controller));
        $controller = preg_replace_callback('/[A-Z]/', function ($m) {return '-' . strtolower($m[0]);}, $controller);
        return $controller;
    }
}