<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 10.05.2015
 * Time: 18:58
 */

namespace Pentity2\Infrastructure\Mvc\View\Helper\Translate;


use Zend\I18n\Translator\Translator;
use Zend\View\Helper\AbstractHelper;

class TranslateHelper extends AbstractHelper
{
    private $_translator;

    public function __construct(Translator $translator)
    {
        $this->_translator = $translator;
    }

    public function __invoke($message, $textDomain = 'default')
    {
        return $this->_translator->translate($message, $textDomain, $this->_translator->getLocale());
    }

    public function getTranslator()
    {
        return $this->_translator;
    }
}

