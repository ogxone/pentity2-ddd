<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 05.04.2015
 * Time: 17:26
 */

namespace Pentity2\Infrastructure\Service\DbSession;


use Pentity2\Application\Service\TransactionalSessionInterface;
use Zend\Db\Adapter\Adapter;

class ZfDbSessionService implements TransactionalSessionInterface
{
    private $_connection;

    public function __construct(Adapter $adapter)
    {
        $this->_connection = $adapter->getDriver()->getConnection();
    }

    /**
     * @throws \Exception
    */
    public function executeTransaction(callable $action)
    {
        $this->_connection->beginTransaction();
        try {
            $outcome = $action();
            $this->_connection->commit();
            return $outcome;
        } catch (\Exception $e) {
            $this->_connection->rollback();
            throw $e;
        }
    }
}