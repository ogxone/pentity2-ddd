<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 09.09.15
 * Time: 14:30
 */

namespace Pentity2\Domain\Service\Builder\Assembler\Factory;


use Pentity2\Domain\Service\Builder\Builder\BuilderInterface;
use Pentity2\Domain\Service\Exception\ServiceException;
use Pentity2\Utils\Param\Param;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\MutableCreationOptionsInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

abstract class AbstractAssemblerFactory implements FactoryInterface, MutableCreationOptionsInterface
{
    protected $_creationOptions = [];

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $builder = $this->_createAssemblerLogic($serviceLocator);
        return $builder;
    }

    /**
     * Set creation options
     *
     * @param  array $options
     * @return void
     */
    public function setCreationOptions(Array $options)
    {
        $this->_creationOptions = $options;
    }

    /**
     * @throws ServiceException
     * @return BuilderInterface
     */
    protected function _getBuilder()
    {
        $builder = Param::strict('builder', $this->_creationOptions, 'builder param have to be provided for Assembler');
        if (!$builder instanceof BuilderInterface) {
            throw new ServiceException('builder have to implement Pentity2\Domain\Service\Builder\Builder\BuilderInterface');
        }
        return $builder;
    }

    protected abstract function _createAssemblerLogic(ServiceLocatorInterface $sl);
}