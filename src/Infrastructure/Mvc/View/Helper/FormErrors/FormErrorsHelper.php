<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 26.05.2015
 * Time: 4:47
 */

namespace Pentity2\Infrastructure\Mvc\View\Helper\FormError;

use Zend\Form\FormInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\View\Helper\AbstractHelper;

class FormErrorsHelper extends AbstractHelper implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    public function __invoke(FormInterface $form)
    {
        return $this->renderMessages($form);
    }

    public function renderMessages(FormInterface $form)
    {
        $translator = $this->getServiceLocator()->getServiceLocator()->get('Translator');
        $messages = $form->getInputFilter()->getMessages();
        if (empty($messages)) {
            return '';
        }
        $str = '<div class="alert alert-dismissible alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                 &times;
             </button><ul>';
        foreach ($messages as $field) {
            foreach ($field as $type => $message) {
                $str .= '<li>' . $translator->translate($message) . '</li>';
            }
        }

        $str .= '</ul></div>';
        return $str;

//        var_dump($messages);
    }
}