<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 18.08.15
 * Time: 21:17
 */

namespace Pentity2\Infrastructure\Zf2\Db\Sql;


use Zend\Db\Adapter\Platform\PlatformInterface;
use Zend\Db\Sql\Insert as ZendInsert;

class Insert extends ZendInsert
{
    /**
     * @var bool
     */
    protected $_ignore = false;

    public function getSqlString(PlatformInterface $adapterPlatform = null)
    {
        if ($this->_ignore !== true) {
            return parent::getSqlString($adapterPlatform);
        }
        return preg_replace('/^INSERT/', 'INSERT IGNORE', parent::getSqlString($adapterPlatform), 1);
    }

    public function ignore()
    {
        $this->_ignore = true;
        return $this;
    }

    public function isIgnore()
    {
        return $this->_ignore === true;
    }

}