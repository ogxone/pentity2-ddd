<?php
/**
 * Created by PhpStorm.
 * User: Оля
 * Date: 25.04.2015
 * Time: 15:00
 */

namespace Pentity2\Infrastructure\Mvc\View\Helper\CDN;
use Zend\View\Helper\AbstractHelper;
use Pentity2\Infrastructure\Service\CDN\CDNService;

class CDNHelper extends AbstractHelper
{
    public function __construct(CDNService $cdnService)
    {
        $this->_cdnService = $cdnService;
    }

   public function generateCdnUrl($fileName, $buildPath, $buildUrl, $buildFile, $type, $num = false)
   {
       if (file_exists($buildPath . '/' . '/' . $buildFile)) {
           return $this->_cdnService->generateCdnUrl($fileName, $type, $num = false);
       } else {
           return $buildUrl . '/' . $type .  '/' . $fileName;
       }
   }
}