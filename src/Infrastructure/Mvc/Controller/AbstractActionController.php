<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 06.04.2015
 * Time: 22:59
 */

namespace Pentity2\Infrastructure\Mvc\Controller;


use Pentity2\Infrastructure\Mvc\Controller\Plugin\Acl\Acl;
use Pentity2\Infrastructure\Mvc\Forms\Factory\FormFactory;
use Pentity2\Infrastructure\Repo\Factory\RepoFactory;
use Pentity2\Infrastructure\Service\MCA\MCAService;
use Zend\Http\Request as HttpRequest;
use Zend\I18n\Translator\Translator;
use Zend\Mvc\Controller\AbstractActionController as  ZendAbstractActionController;
use Zend\Mvc\MvcEvent;
use Zend\Http\Response as HttpResponse;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

abstract class AbstractActionController extends ZendAbstractActionController
{
    protected $_isAjaxRequest;

    /**
     * @param MvcEvent $e
     * @throws \Exception
     * @return mixed
     */
    public function onDispatch(MvcEvent $e)
    {
        try {
            $request = $e->getRequest();
            if ($request instanceof HttpRequest) {
                $this->_isAjaxRequest = $request->isXmlHttpRequest();
            }
            $routeMatch = $e->getRouteMatch();
            if (!$routeMatch) {
                throw new \DomainException('Missing route matches; unsure how to retrieve action');
            }
            //acl logic
            /**@var $acl Acl*/
            $acl = $this->getServiceLocator()
                ->get('App\Acl');
            $role = $this->getConfig()['acl']['default_role'];
            if ($user = $this->identity()) {
                $role = $user->role;
            }
            if (!$acl->isAllowed($role, $mca = $this->getMCAService()->getMCA())) {
                throw new \Exception(sprintf('Access forbidden. Role: %s, resource:%s', $role, $mca), 403);
            }
            //default dispatch logic
            $action = $routeMatch->getParam('action', 'not-found');
            $method = static::getMethodFromAction($action);
            if (!method_exists($this, $method)) {
                $method = 'notFoundAction';
            }
            $response = $this->$method();
            $this->layout()->setVariables(['pageId' => $mca]);
        } catch (\Exception $ex) {
            if (!$this->_isAjaxRequest) {
                if (ENVIRONMENT !== 'production') {
                    throw $ex;
                } else {
                    $response = $this->notFoundAction();
                }
            } else {
                $response = new JsonModel([
                    'status' => false,
                    'message' => $ex->getMessage()
                ]);
            }
        }
        $e->setResult($response);
        return $response;
    }

    /**
     * Create an HTTP view model representing a "not found" page
     *
     * @param  HttpResponse $response
     * @return ViewModel
     */
    public function createHttpNotFoundModel(HttpResponse $response)
    {
        $response->setStatusCode(404);
        if ($this->_isAjaxRequest) {
            return new JsonModel([
                'status' => false,
                'message' => 'Page not found'
            ]);
        } else {
            return new ViewModel([
                'content' => 'Page not found'
            ]);
        }
    }

    /**
     * @return Translator
     */
    protected function getTranslator()
    {
        return $this->getServiceLocator()
            ->get('App\Translator');
    }

    public function translate($code)
    {
        return $this->getTranslator()
            ->translate($code);
    }

    /**
     * @param bool $useCache
     * @return RepoFactory
     */
    protected function getRepoFactory($useCache = false)
    {
        $serviceName = $useCache ? 'repoFactoryC' : 'repoFactory';
        return $this->getServiceLocator()
            ->get($serviceName);
    }

    /**
     * @param bool $useCache
     * @return FormFactory
     */
    protected function getFormFactory($useCache = false)
    {
        $serviceName = $useCache ? 'formFactoryC' : 'formFactory';
        return $this->getServiceLocator()
            ->get($serviceName);
    }

    /**
     * @return MCAService
     */
    protected function getMCAService()
    {
        return $this->getServiceLocator()
            ->get('MCA');

    }

    /**
     * @return array|object
     */
    protected function getConfig()
    {
        return $this->getServiceLocator()
            ->get('Config');
    }

    /**
     * @return bool
     */
    protected function isAjaxRequest()
    {
        return $this->_isAjaxRequest;
    }
}