<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 08.09.15
 * Time: 20:24
 */

namespace Pentity2\Domain\Service\Builder\Assembler;


use Pentity2\Domain\Service\Builder\Builder\BuilderInterface;

interface AssemblerInterface
{
    /**
     * @param array $params
     * @return BuilderInterface
     */
    public function assemble(Array $params =[]);
}