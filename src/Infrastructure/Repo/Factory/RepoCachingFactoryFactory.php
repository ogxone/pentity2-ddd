<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 12.05.2015
 * Time: 10:57
 */

namespace Pentity2\Infrastructure\Repo\Factory;


use Zend\Cache\PatternFactory;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class RepoCachingFactoryFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $factory = new RepoFactory();
        $factory->setServiceLocator($serviceLocator);
        $objectCache = PatternFactory::factory('object', [
            'object' => $factory,
            'storage' => $serviceLocator->get('Cache\Repo')
        ]);
        return new RepoCachingFactory($objectCache);
    }
}