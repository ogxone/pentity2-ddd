<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 08.05.2015
 * Time: 4:56
 */

namespace Pentity2\Infrastructure\Mvc\Module;

abstract class AbstractModule
{
    protected function _mergeTemplateMapToConfig($config, $externalPath)
    {
        if (file_exists($externalPath)) {
            $templateMap = include $externalPath;
            if (!isset($config['view_manager']['template_map'])) {
                $config['view_manager']['template_map'] = $templateMap;
            } else {
                $config['view_manager']['template_map'] = array_merge(
                    $config['view_manager']['template_map'],
                    include $externalPath
                );
            }
        }
        return $config;
    }
} 