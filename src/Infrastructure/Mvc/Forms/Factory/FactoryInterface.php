<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 12.05.2015
 * Time: 12:49
 */

namespace Pentity2\Infrastructure\Mvc\Forms\Factory;


interface FactoryInterface
{
    public function createForm($name, Array $params = []);
    public function createInputFilter($name, Array $params = []);
}