<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 04.09.15
 * Time: 13:12
 */

namespace Pentity2\Infrastructure\Cache;



class MasterSlaveCache extends TaggableCache implements MasterSlaveCacheInterface
{
    const MASTER_CACHE_ID = 'internal-master_cache_version';

    public function setItem($key, $value, Array $tags = [])
    {
        $this->_validateKey($key);
        $masterCacheVersion = $this->getMasterCacheVersion();
        $tags = $this->_getTagsData($tags, true);
        $resMaster = $this->_cache->setItem($key . '_master', $this->_serialize([
            'value' => $value,
            'master_cache_version' => $masterCacheVersion,
            'tags' => $tags
        ]));
        $resSlave = $this->_cache->setItem($key . '_slave', $this->_serialize([
            'value' => $value,
            'tags' => $tags
        ]));
        return false !== $resMaster && false !== $resSlave;
    }

    public function setItems(Array $keyValuePairs, Array $tags = [])
    {
        $failures = [];
        $masterCacheVersion = $this->getItem(self::MASTER_CACHE_ID);
        $tags = $this->_getTagsData($tags, true);
        foreach ($keyValuePairs as $key => $value) {
            $this->_validateKey($key);
            $resMaster = $this->_cache->setItem($key . '_master', [
                'value' => $value,
                'master_cache_version' => $masterCacheVersion,
                'tags'  => $tags
            ]);
            $resSlave = $this->_cache->setItem($key . '_slave', [
                'value' => $value,
                'tags'  => $tags
            ]);
            if (false === $resMaster || false === $resSlave) {
                $failures[$key];
            }
        }
        return $failures;
    }

    public function getItem($key, & $success = null, & $casToken = null)
    {
        if (null === ($cache = parent::getItem($key . '_master', $success, $casToken))) {
            if (null === ($cache = parent::getItem($key . '_slave', $success, $casToken))) {
                return null;
            } else {
                //set master cache by hand
                $this->_cache->setItem($key . '_master', $this->_serialize([
                    'value' => $cache,
                    'master_cache_version' => $this->getMasterCacheVersion(),
                    'tags' => $this->getTags($key . '_slave')
                ]));
                return null;
            }
        }
        return $cache;
    }

    public function _isExpired(Array $itemData)
    {
        if (
            isset($itemData['master_cache_version']) && //do not check master cache for slave
            $itemData['master_cache_version'] < $this->getMasterCacheVersion()
        ) {
            return true;
        }
        return parent::_isExpired($itemData);
    }

    public function incrementMasterCacheVersion()
    {
        $newVersion = $this->_createNewTagVersion();
        $res = $this->_cache->setItem(self::MASTER_CACHE_ID, $newVersion);
        if ($res) {
            return $newVersion;
        } return null;
    }

    public function getMasterCacheVersion()
    {
        if (null !== ($version = $this->_cache->getItem(self::MASTER_CACHE_ID))) {
            return $version;
        }
        return  $this->incrementMasterCacheVersion();
    }
}