<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 30.03.2015
 * Time: 1:24
 */

namespace Pentity2\Domain\Event;


interface DomainEventSubscriberInterface
{
    public function handle(DomainEventInterface $event);
    public function isSubscribedTo(DomainEventInterface $event);
} 