<?php
/**
 * Created by PhpStorm.
 * User: Оля
 * Date: 25.04.2015
 * Time: 15:09
 */

namespace Pentity2\Infrastructure\Mvc\View\Helper\MCA\Factory;


use Pentity2\Infrastructure\Mvc\View\Helper\MCA\MCAHelper;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class MCAFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return new MCAHelper(
            $serviceLocator->getServiceLocator()
                ->get('MCA')
        );
    }
}