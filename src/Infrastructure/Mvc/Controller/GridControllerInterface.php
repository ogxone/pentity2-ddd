<?php
/**
 * Created by PhpStorm.
 * User: vov4ik
 * Date: 29.05.15
 * Time: 11:53
 */

namespace Pentity2\Infrastructure\Mvc\Controller;


interface GridControllerInterface
{
    function createAction();
    function updateAction();
    function deleteAction();
}