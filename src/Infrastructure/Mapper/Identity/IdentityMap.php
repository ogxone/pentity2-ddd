<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 29.03.2015
 * Time: 21:35
 */

namespace Pentity2\Infrastructure\Mapper\Identity;

use Pentity2\Domain\Entity\EntityInterface;
use Pentity2\Infrastructure\Mapper\Identity\Exception\IdentityException;

class IdentityMap implements IdentityInterface
{
    protected  $_map = [];

    public function addToMap($obj, $id)
    {
        $this->_map[get_class($obj) . '.' . $id] = $obj;
    }

    public function addEntityToMap(EntityInterface $entity)
    {
        if ($entity->isNewRecord()) {
            throw new IdentityException(sprintf('Can\'t add new entity to identity map. Entity class:%s', get_class($entity)));
        }
        $this->addToMap($entity, $entity->getIdField());
    }

    public function getIdentity($type, $id)
    {
        if (is_object($type)) {
            $type = get_class($type);
        }
        $key = $type . '.' . $id;
        if (false === isset($this->_map[$key])) {
            return false;
        }
        return $this->_map[$key];
    }

    public function getIdentityByEntity(EntityInterface $entity)
    {
        if (!$entity->isNewRecord()) {
            return $this->getIdentity(
                get_class($entity),
                $entity->getField($entity->getIdFieldName()));
        }
        return false;
    }
} 