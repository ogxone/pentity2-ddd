<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 21.05.2015
 * Time: 13:13
 */

namespace Pentity2\Infrastructure\Mvc\View\Helper\Mesasge;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

use Zend\View\Helper\AbstractHelper;

class MessageHelper extends AbstractHelper implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    const TYPE_ERROR = 'error';
    const TYPE_INFO = 'info';
    const TYPE_DEFAULT = 'default';
    const TYPE_SUCCESS = 'success';

    private $_initialized = false;
    private $_flash;

    public function _init()
    {
        if ($this->_initialized) {
            return;
        }
        $this->_flash = $this->getServiceLocator()->get('flashMessenger');
//        $this->_flash->setMessageOpenFormat('<div%s>
//             <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
//                 &times;
//             </button>
//             <ul><li>')
        $this->_flash->setMessageOpenFormat('<div%s>

             <ul><li>')
            ->setMessageSeparatorString('</li><li>')
            ->setMessageCloseString('</li></ul></div>');
        $this->_initialized = true;
    }

    public function __invoke()
    {
        return $this->renderMessages();
    }

    public function renderMessages()
    {
        $this->_init();
        if ($this->_flash->hasMessages()) {
            return  $this->_flash->render('default', array('alert', 'alert-dismissible', 'alert-warning'));
        }
        if ($this->_flash->hasInfoMessages()) {
            return  $this->_flash->render('info',    array('alert', 'alert-dismissible', 'alert-info'));
        }
        if ($this->_flash->hasSuccessMessages()) {
            return $this->_flash->render('success', array('alert', 'alert-dismissible', 'alert-success'));
        }
        if ($this->_flash->hasWarningMessages() || $this->_flash->hasErrorMessages()) {
            return  $this->_flash->render('error', array('alert', 'alert-dismissible', 'alert-danger'));
        }
    }
}