<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 05.04.2015
 * Time: 20:29
 */
return [
    'service_manager' => [
        'factories' => [
            'ModulesList'                                                                   => '\Pentity2\Infrastructure\Factory\ModulesListFactory',

            '\Pentity2\Infrastructure\Factory\RepoCachingFactory'                           => '\Pentity2\Infrastructure\Repo\Factory\RepoCachingFactoryFactory',
            '\Pentity2\Infrastructure\Mvc\Forms\Factory\FormCachingFactory'                 => '\Pentity2\Infrastructure\Mvc\Forms\Factory\FormCachingFactoryFactory',

            '\Pentity2\Infrastructure\Mail\Factory\Mail'                                    => '\Pentity2\Infrastructure\Mail\Factory\MailFactory',
            '\Pentity2\Infrastructure\Mapper\Registry\MapperRegistry'                       => '\Pentity2\Infrastructure\Mapper\Registry\Factory\MapperRegistryFactory',
            '\Pentity2\Infrastructure\TransactionManager\TransactionManager'                => '\Pentity2\Infrastructure\Service\TransactionManager\Factory\TransactionManagerFactory',
        ],
        'abstract_factories' => [
            'Zend\Log\LoggerAbstractServiceFactory'
        ],
        'invokables' => [
            '\Pentity2\Infrastructure\Factory\RepoFactory'                                   => '\Pentity2\Infrastructure\Repo\Factory\RepoFactory',
            '\Pentity2\Infrastructure\Service\Factory\InfrastructureFactory'                 => '\Pentity2\Infrastructure\Service\Factory\InfrastructureFactory',
            '\Pentity2\Application\Service\Factory\ApplicationFactory'                       => '\Pentity2\Application\Service\Factory\ApplicationFactory',
            '\Pentity2\Domain\Service\Factory\ServiceFactory'                                => '\Pentity2\Domain\Service\Factory\ServiceFactory',
            '\Pentity2\Infrastructure\Mvc\Forms\Factory\FormFactory'                         => '\Pentity2\Infrastructure\Mvc\Forms\Factory\FormFactory',

            '\Pentity2\Domain\Entity\Factory\Resolver\DefaultResolver'                       => '\Pentity2\Domain\Entity\Factory\Resolver\DefaultResolver',

            '\Pentity2\Infrastructure\Mapper\Identity\IdentityMap'                           => '\Pentity2\Infrastructure\Mapper\Identity\IdentityMap',
            '\Pentity2\Infrastructure\Mapper\MapperManager'                                  => '\Pentity2\Infrastructure\Mapper\Manager\MapperManager',

            '\Pentity2\Infrastructure\Cache\Delegators\MasterSlaveCache'                     => '\Pentity2\Infrastructure\Cache\Delegators\MasterSlaveCacheDelegator',
            '\Pentity2\Infrastructure\Cache\Delegators\TaggableCache'                        => '\Pentity2\Infrastructure\Cache\Delegators\TaggableCacheDelegator'
        ],
        'aliases' => [
            'Repo\Factory'                                                                  => '\Pentity2\Infrastructure\Factory\RepoFactory',
            'Repo\FactoryC'                                                                 => '\Pentity2\Infrastructure\Factory\RepoCachingFactory',

            'Infrastructure\ServiceFactory'                                                 => '\Pentity2\Infrastructure\Service\Factory\InfrastructureFactory',
            'Domain\ServiceFactory'                                                         => '\Pentity2\Domain\Service\Factory\ServiceFactory',
            'Application\ServiceFactory'                                                    => '\Pentity2\Application\Service\Factory\ApplicationFactory',

            'Form\Factory'                                                                  => '\Pentity2\Infrastructure\Mvc\Forms\Factory\FormFactory',
            'Form\FactoryC'                                                                 => '\Pentity2\Infrastructure\Mvc\Forms\Factory\FormCachingFactory',

            'Mapper\Registry'                                                               => '\Pentity2\Infrastructure\Mapper\Registry\MapperRegistry',
            'Mapper\Manager'                                                                => '\Pentity2\Infrastructure\Mapper\MapperManager',
            'Mapper\IdentityMap'                                                            => '\Pentity2\Infrastructure\Mapper\Identity\IdentityMap',

            'Db\TransactionManager'                                                         => '\Pentity2\Infrastructure\TransactionManager\TransactionManager',

            'Delegator\TaggableCache'                                                       => '\Pentity2\Infrastructure\Delegators\TaggableCache',
            'Delegator\MasterSlaveCache'                                                    => '\Pentity2\Infrastructure\Delegators\MasterSlaveCache'
        ]
    ],
    'db' => [
        'adapters' => [
            'Db' => [
                'driver' => 'Pdo',
                'driver_options' => [
                    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
                ],
                'dsn' => 'mysql:dbname=',
                'username' => '',
                'password' => ''
            ]
        ]
    ],

    'mail' => [
        'message' => [
            'from' => 'no-reply@pentity',
            'from_name' => 'pentity'
        ],

        'transport' => [
            'name' => 'sendmail',
            'options' => [

            ]
        ]
    ]
];