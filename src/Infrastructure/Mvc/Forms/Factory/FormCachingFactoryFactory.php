<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 12.05.2015
 * Time: 12:25
 */

namespace Pentity2\Infrastructure\Mvc\Forms\Factory;

use Pentity2\Infrastructure\Mvc\Forms\Factory\FormCachingFactory;
use Pentity2\Infrastructure\Mvc\Forms\Factory\FormFactory;
use Zend\Cache\PatternFactory;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class FormCachingFactoryFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $factory = new FormFactory;
        $factory->setServiceLocator($serviceLocator);
        $objectCache = PatternFactory::factory('object', [
            'object' => $factory,
            'storage' => $serviceLocator->get('Cache\Form')
        ]);
        return new FormCachingFactory($objectCache);
    }
}
