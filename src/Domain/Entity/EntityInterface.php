<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 29.03.2015
 * Time: 20:53
 */

namespace Pentity2\Domain\Entity;


use Pentity2\Utils\Identifier\IdentifierCreatorInterface;

interface EntityInterface extends  IdentifierCreatorInterface
{
    public function setField($name, $value);
    public function getField($name);
    public function removeField($name);
    public function isNewRecord();

    //for hydration purposes
    public function exchangeArray(Array $data);
    public function getArrayCopy();

    public function getIdFieldName();
    public function getIdField();

    public function isValid();
}