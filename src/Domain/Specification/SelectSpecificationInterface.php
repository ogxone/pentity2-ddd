<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 30.03.2015
 * Time: 0:36
 */

namespace Pentity2\Domain\Specification;

use Pentity2\Domain\Repo\RepoInterface;

interface SelectSpecificationInterface
{
    public function selectSatisfyingFrom(RepoInterface $repo);
} 