<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 04.09.15
 * Time: 20:47
 */

namespace Pentity2\Infrastructure\Cache;


interface MasterSlaveCacheInterface
{
    public function incrementMasterCacheVersion();
    public function getMasterCacheVersion();
}