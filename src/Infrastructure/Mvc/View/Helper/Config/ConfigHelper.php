<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 09.09.15
 * Time: 20:56
 */

namespace Pentity2\Infrastructure\Mvc\View\Helper\Config;

use Zend\View\Helper\AbstractHelper;

class ConfigHelper extends AbstractHelper
{
    protected $_config = [];

    public function __construct($config)
    {
        $this->_config = $config;
    }

    public function __invoke($section = null)
    {
        if (is_null($section)) {
            return $this->_config;
        }
        return $this->_config[$section];
    }
}
