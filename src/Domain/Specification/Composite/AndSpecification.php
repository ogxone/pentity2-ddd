<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 30.03.2015
 * Time: 0:43
 */

namespace Pentity2\Domain\Specification\Composite;

use Pentity2\Domain\Specification\SpecificationInterface;

class AndSpecification extends SpecificationComposite
{
    public function isSatisfiedBy($obj)
    {
        foreach ($this->_specifications as $specification) {
            /**@var $specification SpecificationInterface*/

            if (!$specification->isSatisfiedBy($obj)) {
                return false;
            }
        }
        return true;
    }
} 