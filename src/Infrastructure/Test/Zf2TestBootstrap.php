<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 25.08.15
 * Time: 10:27
 */

namespace Pentity2\Infrastructure\Test;


use Zend\Mvc\Service\ServiceManagerConfig;
use Zend\ServiceManager\ServiceManager;
use Zend\Stdlib\ArrayUtils;

class Zf2TestBootstrap
{
    protected static $serviceManager;
    protected static $config;
    protected static $bootstrap;

    public static function init()
    {
        // Load the user-defined test configuration file, if it exists; otherwise, load
        if (is_readable($confPath = self::findParentPath('vendor') . '/../config/application.test.config.php')) {
            $testConfig = include $confPath;
        } elseif (is_readable($confPath = __DIR__ . '/application.test.config.php')) {
            $testConfig = include $confPath;
        } else {
            $testConfig = [];
        }

        $zf2ModulePaths = [];

        if (isset($testConfig['module_listener_options']['module_paths'])) {
            $modulePaths = $testConfig['module_listener_options']['module_paths'];
            foreach ($modulePaths as $modulePath) {
                if (($path = static::findParentPath($modulePath)) ) {
                    $zf2ModulePaths[] = $path;
                }
            }
        }

        $zf2ModulePaths  = implode(PATH_SEPARATOR, $zf2ModulePaths) . PATH_SEPARATOR;

        static::initAutoloader();

        // use ModuleManager to load this module and it's dependencies
        $baseConfig = [
            'module_listener_options' => [
                'module_paths' => explode(PATH_SEPARATOR, $zf2ModulePaths),
            ]
        ];

        $config = ArrayUtils::merge($baseConfig, $testConfig);


        $serviceManager = new ServiceManager(new ServiceManagerConfig());
        $serviceManager->setService('ApplicationConfig', $config);
        $serviceManager->get('ModuleManager')->loadModules();

        static::$serviceManager = $serviceManager;
        static::$config = $config;
    }

    public static function getServiceManager()
    {
        return static::$serviceManager;
    }

    public static function getConfig()
    {
        return static::$config;
    }

    public static function initAutoloader()
    {
        if (!is_dir($vendorPath = getcwd() . '/vendor')) {
            $vendorPath = static::findParentPath('vendor');
        }
        if (is_readable($vendorPath . '/autoload.php')) {
            include $vendorPath . '/autoload.php';
        } else {
            throw new \Exception('Failed to autoload test environment');
        }
    }

    protected static function findParentPath($path)
    {
        $dir = __DIR__;
        $previousDir = '.';
        while (!is_dir($dir . '/' . $path)) {
            $dir = dirname($dir);
            if ($previousDir === $dir) {
                return false;
            }
            $previousDir = $dir;
        }
        return $dir . '/' . $path;
    }
}