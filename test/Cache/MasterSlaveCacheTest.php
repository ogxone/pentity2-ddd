<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 25.08.15
 * Time: 10:10
 */

namespace Pentity2Test\Cache;


use Pentity2\Infrastructure\Cache\MasterSlaveCache;

use Pentity2\Infrastructure\Test\AbstractTestCase;
use Zend\Cache\StorageFactory;

class MasterSlaveCacheTest extends AbstractTestCase
{
    /**
     * @var $_cache MasterSlaveCache
     */
    protected $_cache;

    public function setUp()
    {
        $decorated = StorageFactory::factory([
            'adapter' => 'memory',
            'options' => [
                'namespace' => 'Cache\Test'
            ],
            'plugins' => [
                'exception_handler' => [
                    'throw_exceptions' => false
                ]
            ]
        ]);
        $this->_cache = new MasterSlaveCache($decorated);
    }

    /**
     * @test
     * @covers MasterSlaveCache::setItem
    */
    public function checkSetItemWorks()
    {
        $cache = $this->_cache;
        $cache->setItem('item1', 'item1Val', ['tag1']);
        //clear cache
        $cache->clearByTags([MasterSlaveCache::MASTER_CACHE_TAG]);

        //first connection fails and generates new cache
        $this->assertEquals($cache->getItem('item1'), false);
        //following connection use slave cache
        $this->assertEquals($cache->getItem('item1'), 'item1Val');
    }
}