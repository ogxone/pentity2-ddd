<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 30.03.2015
 * Time: 0:58
 */

namespace Pentity2\Domain\Specification;


class NotSpecification
{
    protected $_specification;

    public function __construct(SpecificationInterface $spec)
    {
        $this->_specification = $spec;
    }

    public function isSatisfiedBy($obj)
    {
        return !$this->_specification->isSatisfiedBy($obj);
    }
}