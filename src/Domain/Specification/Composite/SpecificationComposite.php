<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 30.03.2015
 * Time: 0:40
 */

namespace Pentity2\Domain\Specification\Composite;

use Pentity2\Domain\Specification\SpecificationInterface;

abstract class SpecificationComposite implements SpecificationInterface
{
    protected $_specifications = [];

    public function __construct(Array $specifications)
    {
        foreach ($specifications as $specification) {
            $this->add($specification);
        }
    }

    public function add(SpecificationInterface $specification)
    {
        array_push($this->_specifications, $specification);
        return $this;
    }
}