<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 04.08.15
 * Time: 19:55
 */

namespace Pentity2\Infrastructure\Mapper\Registry\Factory;


use Pentity2\Infrastructure\Mapper\Registry\MapperRegistry;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class MapperRegistryFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return new MapperRegistry(
            $serviceLocator->get('Mapper\Manager')
        );
    }
}