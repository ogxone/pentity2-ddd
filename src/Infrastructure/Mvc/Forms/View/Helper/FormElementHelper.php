<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 13.05.2015
 * Time: 13:15
 */

namespace Pentity2\Infrastructure\Mvc\Forms\View\Helper;

use Pentity2\Infrastructure\Mvc\Forms\Element\Tag;
use Zend\Form\View\Helper\FormElement as BaseFormElement;
use Zend\Form\ElementInterface;

class FormElementHelper extends BaseFormElement
{
    public function render(ElementInterface $element)
    {
        $renderer = $this->getView();
        if (!method_exists($renderer, 'plugin')) {
            // Bail early if renderer is not pluggable
            return '';
        }

        if ($element instanceof Tag) {
            $helper = $renderer->plugin('formTag');
            return $helper($element);
        }

        return parent::render($element);
    }
}
