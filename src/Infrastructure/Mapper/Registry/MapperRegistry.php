<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 29.03.2015
 * Time: 21:12
 */

namespace Pentity2\Infrastructure\Mapper\Registry;


use Pentity2\Infrastructure\Mapper\AbstractMapper;
use Pentity2\Infrastructure\Mapper\Manager\MapperManager;

class MapperRegistry
{
    private $_registry;
    private $_mapperManager;

    public function __construct(MapperManager $mapperManager)
    {
        $this->_registry        = new \ArrayObject;
        $this->_mapperManager   = $mapperManager;
    }

    /**
     * @param $entity
     * @param $type
     * @param $storage
     * @return AbstractMapper
     */
    public function getMapperForEntity($entity, $type, $storage)
    {
        $key = get_class($entity) . $type . $storage;
        if (!isset($this->_registry[$key])) {
            $mapper = $this->_mapperManager->get($type, [
                'entity' => $entity,
                'storage' => $storage
            ]);
            $this->_registry[$key] = $mapper;
        }
        return $this->_registry[$key];
    }
}