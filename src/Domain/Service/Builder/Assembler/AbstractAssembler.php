<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 08.09.15
 * Time: 20:25
 */

namespace Pentity2\Domain\Service\Builder\Assembler;


use Pentity2\Domain\Service\Builder\Builder\BuilderInterface;
use Pentity2\Infrastructure\Cache\StorageInterface;
use Pentity2\Infrastructure\Repo\AbstractRepo;
use Pentity2\Utils\Param\Param;

abstract class AbstractAssembler implements AssemblerInterface
{
    /**
     * @var BuilderInterface
    */
    protected $_builder;
    /**
     * @var StorageInterface
    */
    protected $_cache;
    protected static $_useCaching = false;

    public function __construct(BuilderInterface $builder, StorageInterface $cache)
    {
        $this->_builder = $builder;
        $this->_cache = $cache;
    }

    /**
     * @return mixed
     */
    public static function getUseCaching()
    {
        return self::$_useCaching;
    }

    /**
     * @param mixed $useCaching
     */
    public static function setUseCaching($useCaching)
    {
        self::$_useCaching = (bool)$useCaching;
    }

    /**
     * @return mixed
     */
    public function getBuilder()
    {
        return $this->_builder;
    }

    /**
     * @param mixed $builder
     */
    public function setBuilder($builder)
    {
        $this->_builder = $builder;
    }

    /**
     * @param array $params
     * @return object
     */
    public function assemble(Array $params = [])
    {
        $caching = Param::get('caching', $params, static::$_useCaching);
        if (!$caching) {
            $this->_assembleLogic();
            $res = $this->_builder->getResult();
        } else {
            $repoCaching = AbstractRepo::isUseCaching();
            AbstractRepo::setUseCaching(false);
            $tags = Param::getArr('tags', $params);
            $key = $this->_createKey($params);
            if (null === ($res = $this->_cache->getItem($key))) {
                $this->_assembleLogic($params);
                $this->_cache->setItem($key, $res = $this->_builder->getResult(), $tags);
            }
            AbstractRepo::setUseCaching($repoCaching);
        }
        return $res;
    }

    private function _createKey(Array $params)
    {
        return str_replace('\\', '_', get_called_class()) . '-' .
            str_replace('\\', '_', get_class($this->_builder)) . '-' .
            md5(serialize($params));
    }

    abstract function _assembleLogic(Array $params = []);
}