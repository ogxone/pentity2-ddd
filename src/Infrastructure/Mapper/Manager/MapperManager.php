<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 05.08.15
 * Time: 11:48
 */

namespace Pentity2\Infrastructure\Mapper\Manager;



use Pentity2\Infrastructure\Mapper\Exception\MapperException;
use Pentity2\Infrastructure\Mapper\MapperInterface;
use Zend\ServiceManager\AbstractPluginManager;
use Zend\ServiceManager\ConfigInterface;
use Zend\ServiceManager\Exception;

class MapperManager extends AbstractPluginManager
{
    public function __construct(ConfigInterface $config = null)
    {
        parent::__construct($config);
        $this->setFactory('sql', 'Pentity2\Infrastructure\Mapper\Manager\Factory\SqlMapperFactory');
    }

    /**
     * Validate the plugin
     *
     * Checks that the filter loaded is either a valid callback or an instance
     * of FilterInterface.
     *
     * @param  mixed $plugin
     * @throws MapperException
     */
    public function validatePlugin($plugin)
    {
        if ($plugin instanceof MapperInterface) {
            return;
        }
        throw new MapperException(sprintf('Mapper instance expected to be of type MapperInterface, %s given', get_class($plugin)));
    }

}