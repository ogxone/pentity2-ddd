<?php
/**
 * Created by PhpStorm.
 * User: Оля
 * Date: 25.04.2015
 * Time: 15:00
 */

namespace Pentity2\Infrastructure\Mvc\View\Helper\MCA;


use Pentity2\Infrastructure\Service\MCA\MCAService;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\View\Helper\AbstractHelper;

class MCAHelper extends AbstractHelper implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait; //@todo refactor this with factory

    private $_mcaService;

    public function __construct(MCAService $mcaService)
    {
        $this->_mcaService = $mcaService;
    }

    public function __invoke()
    {
        return $this->getMCA();
    }

    public function getM()
    {
        return $this->_mcaService->getM();
    }

    public function getMC()
    {
        return $this->_mcaService->getMC();
    }

    public function getMCA()
    {
        return $this->_mcaService->getMCA();
    }
} 