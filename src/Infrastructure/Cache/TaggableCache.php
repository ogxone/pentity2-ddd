<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 22.05.2015
 * Time: 11:18
 */

namespace Pentity2\Infrastructure\Cache;


use Zend\Cache\Storage\Adapter;

class TaggableCache extends AbstractCacheDecorator
{
    const TAGS_KEY = 'internal-tags';

    public function setItem($key, $value, Array $tags = [])
    {
        $this->_validateKey($key);
        return $this->_cache->setItem($key, $this->_serialize([
            'value' => $value,
            'tags'  => $this->_getTagsData($tags, true)
        ]));
    }

    public function setItems(Array $keyValuePairs, Array $tags = [])
    {
        $failures = [];
        $tagsData = $this->_getTagsData($tags, true);
        foreach ($keyValuePairs as $key => $value) {
            $this->_validateKey($key);
            $res = $this->_cache->setItem($key, $this->_serialize([
                'value' => $value,
                'tags'  => $tagsData
            ]));
            if (false === $res) {
                $failures[$key];
            }
        }
        return $failures;
    }

    public function getItem($key, & $success = null, & $casToken = null)
    {
        $itemData = $this->_getData($key, $success, $casToken);
        if ($itemData) {
            return $itemData['value'];
        }
        return null;
    }

    public function getItems(Array $keys)
    {
        $data = [];
        foreach ($keys as $key) {
            if (null !== ($value = $this->getItem($key))) {
                $data[$key] = $value;
            }
        }
        return $data;
    }

    public function getTags($key, & $success = null, & $casToken = null)
    {
        $itemData = $this->_getData($key, $success, $casToken);
        if ($itemData) {
            return $itemData['tags'];
        }
        return null;
    }

    public function clearByTags(Array $tags)
    {
        $modified = false;
        if (null !== ($savedTags = $this->_cache->getItem(self::TAGS_KEY))) {
            $savedTags = $this->_unserialize($savedTags);
            $newVersion = $this->_createNewTagVersion();
            foreach ($tags as $tagName) {
                if (isset($savedTags[$tagName])) {
                    $savedTags[$tagName] = $newVersion;
                    $modified = true;
                }
            }
            if ($modified) {
                $this->_cache->setItem(self::TAGS_KEY, $this->_serialize($savedTags));
            }
        }
    }

    public function hasItem($key)
    {
        if (!$this->_cache->hasItem($key)) {
            return false;
        }
        // check tags expiration
        $item = $this->_cache->getItem($key);
        $itemData = $this->_unserialize($item);
        if ($this->_isExpired($itemData)) {
            return false;
        }
        return true;
    }

    public function hasItems(Array $keys)
    {
        $data = [];
        foreach ($keys as $key) {
            if ($this->hasItem($key)) {
                $data[] = $key;
            }
        }
        return $data;
    }

    private function _getData($key, & $success = null, & $casToken = null)
    {
        if (null === ($item = $this->_cache->getItem($key, $success, $casToken))) {
            return null;
        }
        $itemData = $this->_unserialize($item);
        if ($this->_isExpired($itemData)) {
            return null;
        }
        return $itemData;
    }

    protected function _isExpired(Array $itemData)
    {
        $savedTags = $this->_getTagsData(array_keys($itemData['tags']));
        foreach ($itemData['tags'] as $tagName => $tagVersion) {
            if ($savedTags[$tagName] > $tagVersion) {
                return true;
            }
        }
        return false;
    }

    protected function _getTagsData(Array $tags, $touchTags = false)
    {
        if (null === ($savedTags = $this->_cache->getItem(self::TAGS_KEY))) {
            $savedTags = [];
        } else {
            $savedTags = $this->_unserialize($savedTags);
        }
        $tagsData = [];
        $currentVersion = $this->_createNewTagVersion();
        foreach ($tags as $tagName) {
            if ($touchTags) {
                $savedTags[$tagName] = $currentVersion;
            }
            if (isset($savedTags[$tagName])) {
                $tagsData[$tagName] = $savedTags[$tagName];
            }
        }
        $this->_cache->setItem(self::TAGS_KEY, $this->_serialize($savedTags));

        return $tagsData;
    }

    protected function _createNewTagVersion()
    {
        return microtime(1);
    }
}