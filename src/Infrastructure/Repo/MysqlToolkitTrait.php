<?php
/**
 * Created by PhpStorm.
 * User: Оля
 * Date: 14.04.2015
 * Time: 23:59
 */

namespace Pentity2\Infrastructure\Repo;


use Pentity2\Infrastructure\Mapper\SqlMapperInterface;
use Pentity2\Utils\Param\Param;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\In;
use Zend\Db\Sql\Predicate\Predicate;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;

trait MysqlToolkitTrait
{
    protected function _setFields(SqlMapperInterface $mapper, Array $params)
    {

        $fields = Param::get('fields', $params, ['*']);
        $mapper->getSelect()
            ->columns($fields);
    }

    protected function _applyParams($mapper, Array $params = [])
    {
        foreach ($params as $name => $param) {
            switch (strtolower($name)) {
                case 'where' :
                    $this->_applyWhere($mapper, $param);
                    break;
                case 'like':
                    $this->_applyLike($mapper, $params);
                    break;
                default:
                    if (method_exists($this, $filter = $this->_createFilterName($name))) {
                        if (is_array($param)) {
                            call_user_func_array([$this, $filter], array_merge([$mapper], $param));
                        } else {
                            call_user_func([$this, $filter], $mapper, $param);
                        }
                    }
            }
        }
    }

    protected function _applyWhere(SqlMapperInterface $mapper, $condition, $opType = Where::OP_AND)
    {
        if (
            $condition instanceof Predicate ||
            is_array($condition) ||
            is_string($condition)
        ) {
            $mapper->getSelect()
                ->where($condition, $opType);
        }
    }

    protected function _applyWhereIn(SqlMapperInterface $mapper, $identifier, $values, $opType = Where::OP_AND)
    {
        if (!(
            $values instanceof Select ||
            is_array($values)
        )) {
            $mapper->getSelect()
                ->where(new In($identifier, $values), $opType);
        }
    }

    protected function _applyWhereBetween(SqlMapperInterface $mapper, $identifier, $min, $max, $opType = Where::OP_AND)
    {
        $mapper->getSelect()
            ->where(new Between($identifier, $min, $max), $opType);
    }

    protected function _applyOrder(SqlMapperInterface $mapper, $field, $order = Select::ORDER_ASCENDING)
    {
        if (
            null !== $field &&
            in_array(strtoupper($order), [Select::ORDER_ASCENDING, Select::ORDER_DESCENDING])
        ) {
            $mapper->getSelect()
                ->order([$field => $order]);
        }
    }


    private function _createFilterName($paramName)
    {
        return '_apply' . str_replace(' ', '',
            ucwords(
                preg_replace('/[^a-z\s\d]/si', ' ', $paramName)
            )
        );
    }

    protected function _applyLike(SqlMapperInterface $mapper, $condition)
    {
        if (
            $condition instanceof Predicate ||
            is_array($condition) ||
            is_string($condition) &&
            isset($condition['like'])
        ) {

            foreach ($condition['like'] as $key => $val) {
                $mapper->getSelect()
                    ->where->like($key, $val);
            }

        }
    }
} 