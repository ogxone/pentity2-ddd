<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 29.03.2015
 * Time: 21:19
 */

namespace Pentity2\Application\Service;


interface ExecutableApplicationServiceInterface
{
    public function execute(Array $params = []);
} 