<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 05.08.15
 * Time: 17:03
 */

namespace Pentity2\Infrastructure\Mapper\Manager\Factory;


use Pentity2\Domain\Entity\EntityInterface;
use Pentity2\Infrastructure\Mapper\Exception\MapperException;
use Pentity2\Utils\Param\Param;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\MutableCreationOptionsInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

abstract class AbstractFactory implements FactoryInterface, MutableCreationOptionsInterface
{
    protected $_creationOptions = [];

    public function __construct(Array $creationOptions = [])
    {
        $this->_creationOptions = $creationOptions;
    }

    public function createService(ServiceLocatorInterface $sl)
    {
        $sl = $sl->getServiceLocator();
        $mapper = $this->_createMapperLogic($sl);
        $mapper->setEventManager($sl->get('EventManager'));
        $mapper->setIdentityMap($sl->get('Mapper\IdentityMap'));
        return $mapper;
    }

    abstract protected function _createMapperLogic(ServiceLocatorInterface $sl);

    /**
     * Set creation options
     *
     * @param  array $options
     * @return void
     */
    public function setCreationOptions(Array $options)
    {
        $this->_creationOptions = $options;
    }

    protected function _getEntity()
    {
        $entity = Param::strict('entity', $this->_creationOptions);
        if (!$entity instanceof EntityInterface) {
            throw new MapperException(sprintf('Entity expected to be of type EntityInterface. %s given', get_class($entity)));
        }
        return $entity;
    }

    protected function _getStorage()
    {
        return Param::strict('storage', $this->_creationOptions);
    }
}