<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 05.04.2015
 * Time: 19:48
 */

namespace Pentity2\Infrastructure\Repo\Factory;

use Pentity2\Domain\Entity\EntityInterface;
use Pentity2\Domain\Repo\Exception\RepoException;
use Pentity2\Domain\Repo\Factory\RepoFactoryInterface;
use Pentity2\Domain\Repo\RepoInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;


class RepoFactory  implements RepoFactoryInterface, ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    private $_namespaces;

    /**
     * @param string $name
     * @param array $params
     * @return RepoInterface
     * @throws RepoException
     */
    public function createRepo($name, Array $params = [])
    {
        foreach ($this->_buildNamespaces() as $namespace) {
            if (class_exists($class = $namespace . ucfirst($name) . 'Repo')) {
                $ref = new \ReflectionClass($class);
                $options = [
                    $this->getServiceLocator()->get('Mapper\Registry'),
                    $this->getServiceLocator()->get('Cache\Db'),
                ];
                $options += $params;
                $repo = $ref->newInstanceArgs($options);
                return $repo;
            }
        }
        throw new RepoException(sprintf('Failed to load repo by name %s', $name));
    }

    public function _buildNamespaces()
    {
        if (null === $this->_namespaces) {
            $this->_namespaces = array_map(function ($v) {
                return sprintf('%s\Infrastructure\Repo\\', $v);
            }, $this->getServiceLocator()->get('ModulesList'));
        }
        return $this->_namespaces;
    }
}