<?php
/**
 * Created by PhpStorm.
 * User: Оля
 * Date: 25.04.2015
 * Time: 15:09
 */

namespace Pentity2\Infrastructure\Service\CDN\Factory;

use Pentity2\Infrastructure\Service\CDN\CDNService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class CDNFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return new CDNService(
            $serviceLocator->get('Config')
        );
    }
}