<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 09.09.15
 * Time: 20:50
 */

namespace Pentity2\Infrastructure\Mvc\View\Helper\Params\Factory;


use Pentity2\Infrastructure\Mvc\View\Helper\Params\Params;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ParamsFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $app = $serviceLocator->getServiceLocator()
            ->get('Application');
        return new Params($app->getRequest(), $app->getMvcEvent());
    }
}