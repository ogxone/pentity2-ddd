<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 29.03.2015
 * Time: 23:35
 */

namespace Pentity2\Domain\Entity\Collection;

use Pentity2\Domain\Entity\EntityInterface;

interface EntityCollectionInterface extends \Countable, \ArrayAccess, \IteratorAggregate
{
    public function add(EntityInterface $entity);
    public function remove(EntityInterface $entity);
    public function get($key);
    public function exists($key);
    public function clear();
    public function toArray();
} 