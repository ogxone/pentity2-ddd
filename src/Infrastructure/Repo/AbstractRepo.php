<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 05.04.2015
 * Time: 18:38
 */

namespace Pentity2\Infrastructure\Repo;


use Pentity2\Domain\Entity\AbstractEntity;
use Pentity2\Domain\Entity\EntityInterface;
use Pentity2\Domain\Repo\RepoInterface;
use Pentity2\Infrastructure\Cache\StorageInterface;
use Pentity2\Infrastructure\Mapper\Registry\MapperRegistry;

abstract class AbstractRepo implements RepoInterface
{
    /**
     * @var MapperRegistry
     */
    protected $_mapperRegistry;
    /**
     * @var AbstractEntity
     */
    protected $_entityPrototype;
    /**
     * @var StorageInterface
     */
    protected $_cache;
    protected static $_useCaching = false;

    public function __construct(
        MapperRegistry $mapperRegistry,
        StorageInterface $cache,
        Array $params = []
    )
    {
        $this->_mapperRegistry = $mapperRegistry;
        $this->_cache = $cache;
        $this->_entityPrototype = $this->getEntityPrototype();
    }

    public static function setUseCaching($value)
    {
        static::$_useCaching = (bool)$value;
    }

    public static function isUseCaching()
    {
        return static::$_useCaching;
    }

    /**
     * @param EntityInterface $entity
     * @param null $type
     * @param null $storage
     * @return \Pentity2\Infrastructure\Mapper\AbstractMapper
     */
    protected function _getMapper(EntityInterface $entity = null, $type = null, $storage = null)
    {
        if (null === $entity) {
            $entity = $this->_entityPrototype;
        }
        if (null === $type) {
            $type = $this->getType();
        }
        if (null === $storage) {
            $storage = $this->getStorage();
        }
        return $this->_mapperRegistry
            ->getMapperForEntity($entity, $type, $storage);
    }

    public function getCacheStorage()
    {
        return $this->_cache;
    }

    public function exists($entity)
    {
        return $this->_getMapper()->exists($entity);
    }

    public function save($entity)
    {
        return $this->_getMapper()->save($entity);
    }

    public function delete($entity)
    {
        return $this->_getMapper()->delete($entity);
    }

    /**
     * Decorates all cache calls
     * @param $name
     * @param array $args
     * @return mixed
     */
    public function __call($name, Array $args)
    {
        //do nothing if this is not caching call or if this is not fetch call
        if (0 !== strpos($name, 'cache') || 5 !== strpos($name, 'Fetch')) {
            throw new \BadMethodCallException(sprintf('Method %s was not found in %s', $name, get_called_class()));
        }
        $method = lcfirst(substr($name, 5));
        //failed to find method
        if (!method_exists($this, $method)) {
            throw new \BadMethodCallException(sprintf('Method %s was not found in %s', $name, get_called_class()));
        }
        //just call corresponding fetch method if caching is turned off
        if (!static::$_useCaching) {
            return call_user_func_array([$this, $method], $args);
        }
        //caching logic
        //looking for tags in key tags in last argument
        $tags = [];
        $paramsGuess = end($args);
        if (is_array($paramsGuess) && isset($paramsGuess['tags'])) {
            $tags = is_array($paramsGuess['tags']) ? $paramsGuess['tags'] : [$paramsGuess['tags']];
        }

        $key = $name . '_' . md5(serialize($args));
        if (null === ($res = $this->_cache->getItem($key))) {
            $res = call_user_func_array([$this, $method], $args);
            $this->_cache->setItem($key, $res, $tags);
        }
        return $res;
    }

    abstract public function getType();
    abstract public function getStorage();
    abstract public function getEntityPrototype();
} 