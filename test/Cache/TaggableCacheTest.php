<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 25.08.15
 * Time: 10:10
 */

namespace Pentity2Test\Cache;


use Pentity2\Infrastructure\Cache\TaggableCache;

use Pentity2\Infrastructure\Test\AbstractTestCase;
use Zend\Cache\StorageFactory;

class TaggableCacheTest extends AbstractTestCase
{
    /**
     * @var $_cache TaggableCache
     */
    protected $_cache;

    public function setUp()
    {
        $decorated = StorageFactory::factory([
            'adapter' => 'memory',
            'options' => [
                'namespace' => 'Cache\Test'
            ],
            'plugins' => [
                'exception_handler' => [
                    'throw_exceptions' => false
                ]
            ]
        ]);
        $this->_cache = new TaggableCache($decorated);
    }

    /**
     * @test
    */
    public function checkSetAndGetDependencyWorks()
    {
        $this->_cache->setItem('deptest1', 'deptest1Value', ['tag1', 'tag2']);
        $this->_cache->setItem('deptest2', 'deptest2Value', ['tag1']);

        //cache version have to be expired by tag tag1
        $this->assertEquals($this->_cache->getItem('deptest1'), null);
        //have to be set
        $this->assertEquals($this->_cache->getItem('deptest2'), 'deptest2Value');

        $this->_cache->setItem('deptest1', 'deptest1ValueChanged', ['tag1', 'tag2']);

        //cache version have to be expired by tag
        $this->assertEquals($this->_cache->getItem('deptest2'), null);
    }

    /**
    * @test
    */
    public function checkExpirationByTags()
    {
        $this->_cache->setItem('deptest1', 'deptest1Value', ['tag1', 'tag2']);
        $this->_cache->setItem('deptest2', 'deptest2Value', ['tag1']);
        $this->_cache->setItem('deptest3', 'deptest3Value', ['tag1']);

        $this->_cache->clearByTags(['tag2', 'tag3']);

        $this->assertEquals($this->_cache->getItem('deptest1'), null);
        $this->assertEquals($this->_cache->getItem('deptest2'), null);
        $this->assertEquals($this->_cache->getItem('deptest3'), 'deptest3Value');

        $this->_cache->clearByTags(['tag1']);
        $this->assertEquals($this->_cache->getItem('deptest2'), null);
        $this->assertEquals($this->_cache->getItem('deptest3'), null);
    }

    /**
     * @todo test hasItem hasItems getItems setItems
     * @test
    */
    public function checkSetMultipleItems()
    {

        // check plain setItems
        $this->_cache->setItems(['1' => '1', '2' => '2', '3' => '3']);

        $this->assertEquals($this->_cache->getItem('1'), 1);
        $this->assertEquals($this->_cache->getItem('2'), 2);
        $this->assertEquals($this->_cache->getItem('3'), 3);

        // check setItems with tags (have to be all set)
        $this->_cache->setItems(['1' => '1', '2' => '2', '3' => '3'], ['tag1']);

        $this->assertEquals($this->_cache->getItem('1'), 1);
        $this->assertEquals($this->_cache->getItem('2'), 2);
        $this->assertEquals($this->_cache->getItem('3'), 3);

        // check clear by tags
        $this->_cache->clearByTags(['tag1']);

        $this->assertEquals($this->_cache->getItem('1'), null);
        $this->assertEquals($this->_cache->getItem('2'), null);
        $this->assertEquals($this->_cache->getItem('3'), null);

        //check expiration by tags
        $this->_cache->setItems(['1' => '1', '2' => '2', '3' => '3'], ['tag1']);
        $this->_cache->setItem('2' ,'2new', ['tag1']);

        $this->assertEquals($this->_cache->getItem('1'), null);
        $this->assertEquals($this->_cache->getItem('2'), '2new');
        $this->assertEquals($this->_cache->getItem('3'), null);
    }

    /**
     * @test test setting tags
    */
    public function checkHasItem()
    {
        $this->_cache->setItem('1', '1v');
        $this->_cache->setItem('2', '2v');

        $this->assertEquals($this->_cache->hasItem('1'), true);
        $this->assertEquals($this->_cache->hasItem('2'), true);
        $this->assertEquals($this->_cache->hasItems(['2']), ['2']);
        $this->assertEquals($this->_cache->hasItems(['1']), ['1']);
        $this->assertEquals($this->_cache->hasItems(['1', '2']), ['1', '2']);

        $this->_cache->setItem('1', '1v', ['tag1']);
        $this->_cache->setItem('2', '2v', ['tag1']);

        $this->assertEquals($this->_cache->hasItem('1'), false);
        $this->assertEquals($this->_cache->hasItem('2'), true);
        $this->assertEquals($this->_cache->hasItems(['1']), []);
        $this->assertEquals($this->_cache->hasItems(['2']), ['2']);
        $this->assertEquals($this->_cache->hasItems(['1', '2']), ['2']);

    }

    /**
     * @test test setting tags
     */
    public function checkSettingWithAndWithoutTags()
    {
        $this->_cache->setItem('1', '1v', ['tag1']);
        $this->_cache->setItem('2', '2v', ['tag1']);

        $this->_cache->setItem('1', '1vchanged');

        $this->assertEquals($this->_cache->getItem('1'), '1vchanged');
        $this->assertEquals($this->_cache->getItem('2'), '2v');
    }

    /**
     * @test test setting tags
     */
    public function checkSetTagsIntersection()
    {
        $this->_cache->setItems(['1' => '1', '2' => '2', '3' => '3'], ['tag1']);
        $this->_cache->setItems(['4' => '4', '5' => '5', '6' => '6', '1' => '1changed'], ['tag2']);

        $this->assertEquals($this->_cache->getItem('1'), '1changed');
        $this->assertEquals($this->_cache->getItem('2'), '2');
        $this->assertEquals($this->_cache->getItem('3'), '3');

        $this->_cache->setItems(['1' => '1changed2'], ['tag1']);

        $this->assertEquals($this->_cache->getItem('1'), '1changed2');
        $this->assertEquals($this->_cache->getItem('2'), null);
        $this->assertEquals($this->_cache->getItem('3'), null);
        $this->assertEquals($this->_cache->getItem('4'), '4');
        $this->assertEquals($this->_cache->getItem('5'), '5');
        $this->assertEquals($this->_cache->getItem('6'), '6');
    }
}