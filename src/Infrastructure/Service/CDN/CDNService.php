<?php
/**
 * Created by PhpStorm.
 * User: Оля
 * Date: 04.05.2015
 * Time: 1:49
 */

namespace Pentity2\Infrastructure\Service\CDN;

use Pentity2\Utils\Param\Param;

class CDNService
{
    private $_cdnNum = 0;

    public function __construct($config)
    {
        $this->_config = $config;
    }

    public function generateCdnUrl($fileName, $type, $num = false)
    {
        $s3Config = Param::getArr('aws_s3', $this->_config);
        $baseUrl = sprintf($s3Config['cdn']['base_url'], false === $num ? $this->_cdnNum++ : $num);
        if ($this->_cdnNum == $s3Config['cdn']['count']) {
            $this->_cdnNum = 0;
        }
        return $baseUrl . ($type ? $type . '/' : '') . ($fileName ? $fileName : '');
    }
}