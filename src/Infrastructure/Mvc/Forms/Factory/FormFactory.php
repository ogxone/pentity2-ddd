<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 12.05.2015
 * Time: 9:29
 */

namespace Pentity2\Infrastructure\Mvc\Forms\Factory;

use Pentity2\Application\Service\Exception\ServiceException;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\ServiceManager\ServiceManager;

class FormFactory implements FactoryInterface, ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    /**@var $_cache ServiceManager*/
    protected $_formNamespaces;
    protected $_inputNamespaces;
    protected $_defModulePath='Forms';
    protected $_defInputsModulePath='Forms\Inputs';

    public function __construct()
    {
        $this->_cache = new ServiceManager;
    }

    public function createForm($name, Array $params = [])
    {
        $formName = ucfirst($name) . 'Form';
        foreach ($this->_buildFormNamespaces() as $namespace) {
            if (class_exists($class = $namespace . $formName)) {
                $ref = new \ReflectionClass($class);
                $options['service_locator'] = $this->getServiceLocator();
                $options['form_config'] = $this->getServiceLocator()->get('Config')['form'];
                $formName = strtolower(preg_replace( '/([A-Z])/', '_$1', lcfirst($name)));
                $form = $ref->newInstanceArgs(array_merge([$formName], [$options + $params]));
                return $form;
            }
        }
        throw new ServiceException(sprintf('Failed to load service by name %s', $name));
    }

    public function createInputFilter($name, Array $params = [])
    {
        $inputName = $name . 'InputFilter';
        foreach ($this->_buildInputNamespaces() as $namespace) {
            if (class_exists($class = $namespace . $inputName)) {
                $ref = new \ReflectionClass($class);
                $input = $ref->newInstanceArgs($params);
                if ($input instanceof ServiceLocatorAwareInterface) {
                    $input->setServiceLocator($this->getServiceLocator());
                }
                return $input;
            }
        }
        throw new ServiceException(sprintf('Failed to load service by name %s', $name));
    }

    public function _buildFormNamespaces()
    {
        if (null === $this->_formNamespaces) {
            $this->_formNamespaces = array_map(function ($v) {
                $s = "%s\\".$this->_defModulePath."\\";
                return sprintf($s, $v);
            }, $this->getServiceLocator()->get('ModulesList'));
        }
        return $this->_formNamespaces;
    }

    public function _buildInputNamespaces()
    {
        if (null === $this->_inputNamespaces) {
            $this->_inputNamespaces = array_map(function ($v) {
                $s = "%s\\".$this->_defInputsModulePath."\\";
                return sprintf($s, $v);
            }, $this->getServiceLocator()->get('ModulesList'));
        }
        return $this->_inputNamespaces;
    }
}