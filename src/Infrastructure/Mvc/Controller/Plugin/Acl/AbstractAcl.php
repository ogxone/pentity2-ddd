<?php
/**
 * Created by PhpStorm.
 * User: Оля
 * Date: 03.05.2015
 * Time: 17:11
 */

namespace Pentity2\Infrastructure\Mvc\Controller\Plugin\Acl;

use Pentity2\Infrastructure\Mvc\Controller\Plugin\Acl\Exception\AclException;
use Pentity2\Utils\Param\Param;
use Zend\Permissions\Acl\Acl as ZendAcl;
use Zend\Permissions\Acl\Resource\GenericResource;
use Zend\Permissions\Acl\Role\GenericRole;

abstract class AbstractAcl implements AclInterface
{
    const GROUP_M = 1;
    const GROUP_MC = 2;
    const GROUP_MCA = 3;

    /**@var $_acl ZendAcl*/
    protected $_acl;
    protected $_rolesInheritance;

    public function __construct(
        Array $resources,
        Array $roles,
        Array $rolesInheritance
    )
    {
        $this->_acl = new ZendAcl;
        $this->_rolesInheritance = $rolesInheritance;

        $this->_setResources($resources);

        $this->_setRoles($roles);
        $this->_configureAcl();
    }

    public function getAcl()
    {
        return $this->_acl;
    }

    public function isAllowed($role, $resource)
    {
        if ($this->_acl->hasResource($resource)) {
            return $this->_acl->isAllowed($role, $resource);
        } else {                                                                    //failed to find resource, trying to find parents
            $parentResources = $this->_makeParentResources($resource);
            if (!empty($parentResources)) {
                foreach ($parentResources as $parentResource) {
                    if ($this->_acl->hasResource($parentResource)) {
                        return $this->_acl->isAllowed($role, $parentResource);      // parent resource found. Compare with it
                    }
                }
            }
            return true;                                                            // all dependencies traversed, nothing found
        }
    }

    protected function _setResources(Array $resources)
    {
        foreach ([self::GROUP_M, self::GROUP_MC, self::GROUP_MCA]  as $resourceCode) {
            foreach ($this->_getGroupResources($resourceCode, $resources) as $resource) {
                $this->_acl->addResource(
                    new GenericResource($resource),
                    $this->_getParentResource($resource, $resources)
                );
            }
        }
    }

    protected function _setRoles(Array $roles)
    {
        foreach ($roles as $role) {
            $this->_acl->addRole(
                new GenericRole($role),
                $this->_getParentRole($role)
            );
        }
    }

    abstract protected function _configureAcl();

    private function _getParentResource($name, Array $resources = null)
    {
        if (null === $resources) {
            $resources = $this->_acl->getResources();
        }
        $nameChunks = explode('_', $name);
        if (count($nameChunks) <= 1) {                      // root resource
            return null;
        }
        $parents = [];
        foreach ($resources as $resource) {
            $groupChunks = explode('_', $resource);
            if ($groupLength = count($groupChunks) >= count($nameChunks)) { //this is child or equal resource, not parent
                continue;
            }
            $buf = $nameChunks;
            while (!empty($buf)) {
                array_pop($buf);
                if (
                    $resource != $name &&                       // omit self
                    $groupChunks == $buf
                ) {
                    $parents[count($groupChunks)] = $resource;
                }
            }
        }
        if (!empty($parents)) {
            return max($parents);
        }
        return null;
    }

    protected function _makeParentResources($resource)
    {
        $chunks = explode('_', $resource);
        $result = [];
        for ($i = 0; $i < count($chunks) - 1; $i++) {
            array_push($result, implode('_', array_slice($chunks, 0, $i+1)));
        }
        return $result;
    }

    protected function _getGroupResources($groupType, $resources = null)
    {
        if (null === $resources) {
            $resources = $this->_acl->getResources();
        }

        $validResources = array_filter($resources, function($item) use ($groupType){
            if (count(explode('_', $item)) != $groupType) {
                return false;
            }
            return true;
        });
        $validKeys = array_keys($validResources);
        foreach ($resources as $name => $asset) {
            if (false === (array_search($name, $validKeys))) {
                unset($resources[$name]);
            }
        }
        return $resources;
    }

    protected function _getParentRole($role)
    {
        if (!isset($this->_rolesInheritance[$role])) {
            return null;
        }
        $parents = Param::getArr($role, $this->_rolesInheritance);
        foreach ($parents as $role) {
            if (!$this->_acl->hasRole($role)) {
                $this->_throwAclException(sprintf('ACL: Undefined role %s in roles inheritance', $role));
            }
        }
        return $parents;
    }

    protected function _throwAclException($message, $code = 0)
    {
        if (ENVIRONMENT != 'production') {
            throw new AclException($message);
        }
    }
}