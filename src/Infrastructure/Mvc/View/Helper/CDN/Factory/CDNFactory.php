<?php
/**
 * Created by PhpStorm.
 * User: Оля
 * Date: 25.04.2015
 * Time: 15:09
 */

namespace Pentity2\Infrastructure\Mvc\View\Helper\CDN\Factory;


use Pentity2\Infrastructure\Mvc\View\Helper\CDN\CDNHelper;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class CDNFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return new CDNHelper(
            $serviceLocator->getServiceLocator()
                ->get('CDN')
        );
    }
}