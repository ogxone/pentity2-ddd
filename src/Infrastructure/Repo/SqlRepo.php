<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 30.03.2015
 * Time: 10:02
 */

namespace Pentity2\Infrastructure\Repo;


use Pentity2\Domain\Repo\Exception\RepoException;
use Pentity2\Utils\Param\Param;
use Zend\Db\Sql\Expression;

abstract class SqlRepo extends AbstractRepo
{
    use MysqlToolkitTrait;

    public function fetchById($id, Array $params = [])
    {
        $mapper = $this->_getMapper();
        $this->_setFields($mapper, $params);
        $this->_applyParams($mapper, $params);
        return $mapper->fetchById($id, $params);
    }

    public function fetchOne(Array $params = [])
    {
        $mapper = $this->_getMapper();
        $this->_setFields($mapper, $params);
        $this->_applyParams($mapper, $params);
        return $mapper->fetchOne($params);
    }

    public function fetchAll(Array $params = [])
    {
        $mapper = $this->_getMapper();
        $this->_setFields($mapper, $params);
        $this->_applyParams($mapper, $params);
        return $mapper->fetchAll(
            Param::get('limit', $params),
            Param::get('offset', $params),
            $params
        );
    }

    public function updateByAttribute(array $where, array $set)
    {
        return $this->_getMapper()
            ->updateByAttribute($where, $set);
    }

    public function fetchWithMax($attribute)
    {
        if (!$this->_entityPrototype->hasField($attribute)) {
            throw new RepoException(sprintf('Undefined field name %s for entity %s', $attribute, get_class($this->_entityPrototype)));
        }
        $mapper = $mapper = $this->_getMapper();
        $mapper->getSelect()
            ->columns([
                $attribute => new Expression(sprintf('MAX(%s)', $attribute))
            ]);
        return $mapper->fetchOne();
    }

    public function fetchWithMin($attribute, Array $params = [])
    {
        if (!$this->_entityPrototype->hasField($attribute)) {
            throw new RepoException(sprintf('Undefined field name %s for entity %s', $attribute, get_class($this->_entityPrototype)));
        }
        $mapper = $mapper = $this->_getMapper();
        $mapper->getSelect()
            ->columns([
                $attribute => new Expression(sprintf('MIN(%s)', $attribute))
            ]);
        return $mapper->fetchOne();
    }

    public function getType()
    {
        return 'sql';
    }

    public function getStorage()
    {
        $className = get_called_class();
        $classBase = substr($className, strrpos($className, '\\') + 1);
        $storage = str_replace('Repo', '', $classBase) . 's';

        return strtolower($storage);
    }
}