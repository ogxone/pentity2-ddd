<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 04.08.15
 * Time: 17:21
 */

namespace Pentity2\Infrastructure\Mapper;


interface CallProcedureInterface
{
    public function callProcedure($procedureName, Array $callParams, Array $params = []);
}