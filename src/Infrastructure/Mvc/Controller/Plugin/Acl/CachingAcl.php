<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 22.05.2015
 * Time: 8:26
 */

namespace Pentity2\Infrastructure\Mvc\Controller\Plugin\Acl;

use Zend\Cache\Storage\StorageInterface as CacheStorage;

class CachingAcl implements AclInterface
{
    const ACL_CACHE_KEY = 'app_acl';

    private $_cache;
    private $_acl;

    public function __construct(
        Array $resources,
        Array $roles,
        Array $rolesInheritance,
        Array $rules,
        CacheStorage $cache
    )
    {
        $this->_cache = $cache;
        if (null !== ($acl = $this->_cache->getItem(self::ACL_CACHE_KEY))) {
            $this->_acl = $acl;
        } else {
            $this->_acl = new Acl($resources, $roles, $rolesInheritance, $rules);
            $this->_cache->setItem(self::ACL_CACHE_KEY, $this->_acl);
        }
    }

    public function isAllowed($role, $resource)
    {
        return $this->_acl->isAllowed($role, $resource);
    }

    public function getAcl()
    {
        return $this->_acl->getAcl();
    }

}