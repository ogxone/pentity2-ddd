<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 30.03.2015
 * Time: 0:26
 */

namespace Pentity2\Domain\ValueObject;


class AbstractValueObject implements ValueObjectInterface
{

    public function equals(ValueObjectInterface $value)
    {
        return $this == $value;
    }
}