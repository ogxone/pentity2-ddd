<?php
/**
 * Created by PhpStorm.
 * User: Оля
 * Date: 03.05.2015
 * Time: 20:12
 */

namespace Pentity2\Infrastructure\Mvc\Controller\Plugin\Acl;

use Pentity2\Infrastructure\Mvc\Controller\Plugin\Acl\Exception\AclException;

class Acl extends AbstractAcl
{
    protected $_rules;
    protected $_ruleTypes = ['deny', 'allow'];

    public function __construct(
        Array $resources,
        Array $roles,
        Array $rolesInheritance,
        Array $rules

    )
    {
        $this->_rules = $rules;
        parent::__construct($resources, $roles, $rolesInheritance);
    }
    protected function _configureAcl()
    {
        foreach ($this->_rules as $role => $rules) {
            if (!$this->_acl->hasRole($role)) {
                throw new AclException(
                    sprintf('Undefined role %s in roles inheritance', $role));
            }
            foreach ($rules as $rule => $resources) {
                if (false === array_search($rule, $this->_ruleTypes)) {
                    throw new AclException(
                        sprintf('Undefined rule %s ', $rule));
                }
                foreach ($resources as $resource) {
                    if (empty($resource)) {
                        $this->_acl->$rule($role, null);
                        break;
                    }
                    if ($this->_acl->hasResource($resource)) {
                        $this->_acl->$rule($role, $resource);
                    }
                }
            }
        }
    }
}