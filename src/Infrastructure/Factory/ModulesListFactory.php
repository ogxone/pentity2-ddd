<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 05.04.2015
 * Time: 20:03
 */

namespace Pentity2\Infrastructure\Factory;


use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ModulesListFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $controllerName = $serviceLocator->get('Application')
            ->getMvcEvent()
            ->getRouteMatch()
            ->getParam('controller');
        $moduleName = $this->_getModuleFromControllerClassName($controllerName);
        $moduleManager = $serviceLocator->get('ModuleManager');
        $modules = array_map('ucfirst', array_keys($moduleManager->getLoadedModules()));
        if (
            null !== $moduleName &&
            false !== ($key = array_search($moduleName, $modules))
        ) {
            unset($modules[$key]);
            array_unshift($modules, ucfirst($moduleName));
        }
        array_unshift($modules, 'App');
        //set module common first if exists
        if (false !== ($key = array_search('Common', $modules))) {
            unset($modules[$key]);
            array_unshift($modules, 'Common');
        }
        return $modules;
    }

    /**
     * @param $name
     * @return string|null
     */
    private function _getModuleFromControllerClassName($name)
    {
        $chunks = explode('\\', $name);
        $moduleName = array_shift($chunks);

        return $moduleName;
    }
}