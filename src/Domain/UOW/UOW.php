<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 30.03.2015
 * Time: 9:06
 */

namespace Pentity2\Model\Domain\UOW;


use Pentity2\Domain\UOW\UnitOfWorkInterface;
use Pentity2\Utils\Storage\ObjectStorageInterface;
use Pentity2\Domain\Entity\EntityInterface;
use Pentity2\Infrastructure\Mapper\MapperInterface;

class UnitOfWork implements UnitOfWorkInterface
{
    const STATE_NEW     = "NEW";
    const STATE_CLEAN   = "CLEAN";
    const STATE_DIRTY   = "DIRTY";
    const STATE_REMOVED = "REMOVED";

    protected $_dataMapper;
    protected $_storage;

    public function __construct(MapperInterface $dataMapper, ObjectStorageInterface $storage)
    {
        $this->_dataMapper = $dataMapper;
        $this->_storage = $storage;
    }

    public function getDataMapper()
    {
        return $this->_dataMapper;
    }

    public function getObjectStorage()
    {
        return $this->_storage;
    }

    public function registerNew(EntityInterface $entity)
    {
        $this->registerEntity($entity, self::STATE_NEW);
        return $this;
    }

    public function registerClean(EntityInterface $entity)
    {
        $this->registerEntity($entity, self::STATE_CLEAN);
        return $this;
    }

    public function registerDirty(EntityInterface $entity)
    {
        $this->registerEntity($entity, self::STATE_DIRTY);
        return $this;
    }

    public function registerDeleted(EntityInterface $entity)
    {
        $this->registerEntity($entity, self::STATE_REMOVED);
        return $this;
    }

    protected function registerEntity($entity, $state = self::STATE_CLEAN)
    {
        $this->_storage->attach($entity, $state);
    }

    public function commit()
    {
        foreach ($this->_storage as $entity) {
            switch ($this->_storage[$entity]) {
                case self::STATE_NEW:
                case self::STATE_DIRTY:
                    $this->_dataMapper->save($entity);
                    break;
                case self::STATE_REMOVED:
                    $this->_dataMapper->delete($entity);
            }
        }
        $this->clear();
    }

    public function rollback()
    {

    }

    public function clear()
    {
        $this->_storage->clear();
        return $this;
    }
} 