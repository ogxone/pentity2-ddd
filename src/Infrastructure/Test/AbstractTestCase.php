<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 25.08.15
 * Time: 17:34
 */

namespace Pentity2\Infrastructure\Test;


class AbstractTestCase extends \PHPUnit_Framework_TestCase
{
    protected $_serviceLocator;

    protected function _getServiceManager()
    {
        if (null === $this->_serviceLocator) {
            $this->_serviceLocator = Zf2TestBootstrap::getServiceManager();
        }
        return $this->_serviceLocator;
    }
}
