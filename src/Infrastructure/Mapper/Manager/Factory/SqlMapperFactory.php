<?php
/**
 * Created by PhpStorm.
 * User: ogxone
 * Date: 05.08.15
 * Time: 16:57
 */

namespace Pentity2\Infrastructure\Mapper\Manager\Factory;


use Pentity2\Infrastructure\Mapper\Manager\Mappers\SqlMapper;
use Zend\ServiceManager\ServiceLocatorInterface;


class SqlMapperFactory extends AbstractFactory
{
    protected function _createMapperLogic(ServiceLocatorInterface $sl)
    {
        $mapper = new SqlMapper(
            $this->_getEntity(),
            $this->_getStorage(),
            $sl->get('Db'),
            $sl->get('Db\TransactionManager')
        );
        return $mapper;
    }
}